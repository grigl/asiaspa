var Spec = function(){
    $('.full-spec-link').on('click', function(e){
        e.preventDefault();

        var container = $(this).parents('.spec-item');
        var target = container.find('.spec-full');

        target.show();
    });

    $('.spec-full').on('click', '.modal-close', function(){
        var target = $(this).parents('.spec-full');

        target.hide();
    });
};

var Fancybox = function(){
    $('.fancybox').fancybox({
        padding: 0,
        tpl: {
            closeBtn: '<div class="modal-close"></div>'
        },
        helpers: {
            overlay: {
                opacity: 0.5,
                locked: false,
            }
        }
    });

    $('.fancybox-img').fancybox({
        tpl: {
            closeBtn: '<div class="modal-close"></div>'
        },
        helpers: {
            overlay: {
                opacity: 0.5,
                locked: false,
            }
        }
    });

    $('body').on('submit', '.fancybox-response', function(e){
        e.preventDefault();

        var data = $(this).serialize();
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function(data){
                $.fancybox({
                    padding: 0,
                    tpl: {
                        closeBtn: '<div class="modal-close"></div>'
                    },
                    helpers: {
                        overlay: {
                            opacity: 0.5,
                            locked: false,
                        }
                    },
                    content: data
                });
            }
        });
    });
};

var CategoryLinks = function(){
    $('.category-link').on('click', function(e){
        e.preventDefault();

        var li = $(this).parents('li');
        var ul = $(this).parents('ul');

        if(!li.hasClass('is-expanded')) {
            ul.find('li.is-expanded').removeClass('is-active').removeClass('is-expanded');
            li.addClass('is-active').addClass('is-expanded');
        }
    });
};

var IndexSlider = function(){
    $.each($('.index-slider'), function(){
        var el = $(this);
        var delay = el.attr('data-delay');

        el.bxSlider({
            auto: true,
            mode: 'fade',
            controls: false,
            pager: false,
            padding: '0px',
            pause: 15000,
            autoDelay: delay,
            preloadImages: 'all'
        });

        el.show();
    })
};

$(function(){
    Fancybox();
    Spec();
    CategoryLinks();
    IndexSlider();
});