# -*- coding: utf-8 -*-
from config.base import *
SITE_NAME = u'AsiaSpa'
DEFAULT_FROM_EMAIL = u'robot@octweb.ru'
DATABASE_NAME = 'asiaspa'

try:
    from config.development import *
except ImportError:
    from config.production import *

TEMPLATE_DEBUG = DEBUG

INSTALLED_APPS += (
    'apps.siteblocks',
    'apps.pages',
    # 'apps.faq',
    # 'apps.slider',
    # 'apps.products',
    # 'apps.newsboard',
    'apps.specs',
    'apps.reviews',
    'apps.services',
    'apps.articles',
    # 'apps.orders',
    # 'apps.inheritanceUser',
    'sorl.thumbnail',
    'south',
    #'captcha',
)

MIDDLEWARE_CLASSES += (
    'apps.pages.middleware.PageFallbackMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    # 'apps.auth_backends.CustomUserModelBackend',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'apps.pages.context_processors.meta',
    'apps.siteblocks.context_processors.settings',
    'apps.utils.context_processors.authorization_form',
)