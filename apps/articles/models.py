# -*- coding: utf-8 -*-
import os, datetime
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

from pytils.translit import translify
from django.core.urlresolvers import reverse
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from sorl.thumbnail import ImageField

from apps.siteblocks.models import Settings


def image_file_path(instance, filename):
    return os.path.join('images', instance.__class__.__name__.lower(), translify(filename).replace(' ', '_') )

class Social(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    image = ImageField(verbose_name=u'заглавная картинка', upload_to=image_file_path)
    full_text = models.TextField(verbose_name=u'полный текст')
    created_at = models.DateField(verbose_name=u'дата создания', editable=False, default=datetime.datetime.now())
    ordering = ['-created_at']

    class Meta:
        verbose_name = u'общественная жизнь'
        verbose_name_plural = u'общественная жизнь'

    def __unicode__(self):
        return self.title


class NewsItem(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    short_text = models.TextField(verbose_name=u'короткий текст')
    full_text = models.TextField(verbose_name=u'полный текст')
    show_on_index = models.BooleanField(verbose_name=u'показывать на главной')
    created_at = models.DateField(verbose_name=u'дата создания', editable=False, default=datetime.datetime.now())

    class Meta:
        verbose_name = u'новость'
        verbose_name_plural = u'новости'
        ordering = ['-created_at']

    def __unicode__(self):
        return self.title


class Licence(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    description = models.TextField(verbose_name=u'описание')
    image = ImageField(verbose_name=u'картинка', upload_to=image_file_path)

    class Meta:
        verbose_name = u'лицензия или сертификат'
        verbose_name_plural = u'лицензии или сертификаты'

    def __unicode__(self):
        return self.title
    