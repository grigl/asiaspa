# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Social.short_text'
        db.delete_column('articles_social', 'short_text')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Social.short_text'
        raise RuntimeError("Cannot reverse this migration. 'Social.short_text' and its values cannot be restored.")

    models = {
        'articles.newsitem': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'NewsItem'},
            'created_at': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 8, 14, 0, 0)'}),
            'full_text': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_text': ('django.db.models.fields.TextField', [], {}),
            'show_on_index': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'articles.social': {
            'Meta': {'object_name': 'Social'},
            'created_at': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 8, 14, 0, 0)'}),
            'full_text': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['articles']