# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Social'
        db.create_table('articles_social', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('short_text', self.gf('django.db.models.fields.TextField')()),
            ('full_text', self.gf('django.db.models.fields.TextField')()),
            ('created_at', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('articles', ['Social'])

        # Adding model 'NewsItem'
        db.create_table('articles_newsitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('short_text', self.gf('django.db.models.fields.TextField')()),
            ('full_text', self.gf('django.db.models.fields.TextField')()),
            ('show_on_index', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('created_at', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('articles', ['NewsItem'])


    def backwards(self, orm):
        # Deleting model 'Social'
        db.delete_table('articles_social')

        # Deleting model 'NewsItem'
        db.delete_table('articles_newsitem')


    models = {
        'articles.newsitem': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'NewsItem'},
            'created_at': ('django.db.models.fields.DateField', [], {}),
            'full_text': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_text': ('django.db.models.fields.TextField', [], {}),
            'show_on_index': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'articles.social': {
            'Meta': {'object_name': 'Social'},
            'created_at': ('django.db.models.fields.DateField', [], {}),
            'full_text': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'short_text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['articles']