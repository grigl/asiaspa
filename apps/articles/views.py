# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView, FormView

from models import *


class SocialListView(ListView):
    template_name = '/articles/social_list.html'
    context_object_name = 'social_list'
    model = Social
    paginate_by = 12


class NewsListView(ListView):
    model = NewsItem
    context_object_name = 'news_list'
    paginate_by = 12


class NewsDetailView(DetailView):
    template_name = '/articles/newsitem_detail.html'
    model = NewsItem


class SocialDetailView(DetailView):
    template_name = '/articles/social_detail.html'
    model = Social


class LicenceView(ListView):
    template_name = '/articles/licence_list.html'
    context_object_name = 'licence_list'
    model = Licence