# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor 

from models import *


class ArticleAdminForm(forms.ModelForm):
    full_text = forms.CharField(widget=Redactor, label=u'описание')
    

class NewsItemAdmin(admin.ModelAdmin):
    list_display = ('title','created_at')
    readonly_fields = ('created_at',)
    form = ArticleAdminForm

admin.site.register(NewsItem, NewsItemAdmin)


class SocialAdmin(admin.ModelAdmin):
    list_display = ('title','created_at')
    readonly_fields = ('created_at',)
    form = ArticleAdminForm

admin.site.register(Social, SocialAdmin)


class LicenceAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(Licence, LicenceAdmin)