# -*- coding: utf-8 -*-
import os, datetime
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

from pytils.translit import translify
from django.core.urlresolvers import reverse
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from sorl.thumbnail import ImageField

from apps.siteblocks.models import Settings
from apps.specs.models import Spec
from mailers import *


def image_file_path(instance, filename):
    return os.path.join('images', instance.__class__.__name__.lower(), translify(filename).replace(' ', '_') )


class Category(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)

    class Meta:
        verbose_name = u'раздел'
        verbose_name_plural = u'разделы'

    def __unicode__(self):
        return self.title

    def save(self):
        self.slug = slugify(translify(self.title))
        super(Category, self).save()

    def has_subcategories(self):
        if self.subcategory_set.all().count() > 0:
            has_subcategories = True
        else:
            has_subcategories = False

        return has_subcategories



class SubCategory(models.Model):
    category = models.ForeignKey(Category, verbose_name=u'раздел')
    title = models.CharField(verbose_name=u'название', max_length=150)
    description = models.TextField(verbose_name=u'описание')

    class Meta:
        verbose_name = u'подраздел'
        verbose_name_plural = u'подразделы'

    def __unicode__(self):
        return self.title

    def save(self):
        self.slug = slugify(translify(self.title))
        super(SubCategory, self).save()


class Service(MPTTModel):
    category = models.ForeignKey(SubCategory, verbose_name=u'раздел')
    parent = TreeForeignKey('self', verbose_name=u'родительская услуга', related_name='children', blank=True, null=True, on_delete=models.SET_NULL)
    title = models.CharField(verbose_name=u'название', max_length=150)
    short_description = models.TextField(verbose_name=u'краткое описание')
    description = models.TextField(verbose_name=u'полное описание')
    side_text = models.TextField(verbose_name=u'текст сбоку')
    specs = models.ManyToManyField(Spec, verbose_name=u'специалисты для вопросов', null=True, blank=True)

    class Meta:
        verbose_name = u'услуга'
        verbose_name_plural = u'услуги'

    def __unicode__(self):
        return self.title

    def save(self):
        self.slug = slugify(translify(self.title))
        super(Service, self).save()

    def get_subservices(self):
        subservice_list = self.get_children()

        return subservice_list

    def has_subservices(self):
        subservice_list_count = self.get_subservices().count()

        if subservice_list_count > 0:
            has_subservices = True
        else:
            has_subservices = False

        return has_subservices

    def get_prices(self):
        price_list = self.price_set.all()

        return price_list

    def has_prices(self):
        price_count = self.get_prices().count()

        if price_count > 0:
            has_prices = True
        else:
            has_prices = False

        return has_prices

    def has_specs(self):
        specs = self.spec_set.all()

        if specs.count() > 0:
            has_specs = True
        else: 
            has_specs = False

        return has_specs

    
    
class Price(models.Model):
    service = models.ForeignKey(Service, verbose_name=u'услуга')
    title = models.CharField(verbose_name=u'название', max_length=150)
    price = models.CharField(verbose_name=u'цена', max_length=150)

    class Meta:
        verbose_name = u'цена услуги'
        verbose_name_plural = u'цены услуг'
    
    def __unicode__(self):
        return self.title


class ServiceSign(models.Model):
    service = models.ForeignKey(Service, verbose_name=u'услуга')
    initials = models.CharField(verbose_name=u'инициалы', max_length=150)
    phone = models.CharField(verbose_name=u'телефон', max_length=150)
    desired_date = models.CharField(verbose_name=u'желаемая дата', max_length=150)
    desired_time = models.CharField(verbose_name=u'желаемое время', max_length=150)

    class Meta:
        verbose_name = u'запись на процедуру'
        verbose_name_plural = u'записи на процедуры'

    def __unicode__(self):
        self.initials = u'' + self.service + u' ' + self.desired_date

    def save(self, **kwargs):
        if self.pk is None:
            send_service_signup_email(self)

        super(ServiceSign, self).save()


class RecomendationType(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)

    class Meta:
        verbose_name = u'тип рекомендации'
        verbose_name_plural = u'типы рекомендаций'

    def __unicode__(self):
        return self.title

    
class Month(models.Model):
    title = models.CharField(verbose_name=u'название', max_length=150)
    order = models.IntegerField(verbose_name = u'Порядок сортировки', default = 10)

    class Meta:
        verbose_name = u'месяц'
        verbose_name_plural = u'месяц'
        ordering = ['-order']

    def __unicode__(self):
        return self.title

    @classmethod
    def get_current_month(self):
        current_month = datetime.date.today().month

        return current_month

    
class Recomendation(models.Model):
    service = models.ForeignKey(Service, verbose_name=u'услуга')
    month = models.ForeignKey(Month, verbose_name=u'месяц')
    type = models.ForeignKey(RecomendationType, verbose_name=u'тип рекомендации')

    class Meta:
        verbose_name = u'рекомендация'
        verbose_name_plural = u'рекомендации'

    def __unicode__(self):
        return u''
    
    
            
    