# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Category.slug'
        db.delete_column('services_category', 'slug')

        # Deleting field 'SubService.slug'
        db.delete_column('services_subservice', 'slug')

        # Deleting field 'Service.slug'
        db.delete_column('services_service', 'slug')

        # Deleting field 'SubCategory.slug'
        db.delete_column('services_subcategory', 'slug')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Category.slug'
        raise RuntimeError("Cannot reverse this migration. 'Category.slug' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'SubService.slug'
        raise RuntimeError("Cannot reverse this migration. 'SubService.slug' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Service.slug'
        raise RuntimeError("Cannot reverse this migration. 'Service.slug' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'SubCategory.slug'
        raise RuntimeError("Cannot reverse this migration. 'SubCategory.slug' and its values cannot be restored.")

    models = {
        'services.category': {
            'Meta': {'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'services.price': {
            'Meta': {'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.Service']", 'null': 'True', 'blank': 'True'}),
            'subservice': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.SubService']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'services.service': {
            'Meta': {'object_name': 'Service'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.SubCategory']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'short_description': ('django.db.models.fields.TextField', [], {}),
            'side_text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'services.subcategory': {
            'Meta': {'object_name': 'SubCategory'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.Category']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'services.subservice': {
            'Meta': {'object_name': 'SubService'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.Service']"}),
            'short_description': ('django.db.models.fields.TextField', [], {}),
            'side_text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['services']