# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'SubService'
        db.delete_table('services_subservice')

        # Adding model 'SeviceSign'
        db.create_table('services_sevicesign', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('service', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['services.Service'])),
            ('initials', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('desired_date', self.gf('django.db.models.fields.DateField')()),
            ('desired_time', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal('services', ['SeviceSign'])

        # Adding field 'Service.parent'
        db.add_column('services_service', 'parent',
                      self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, on_delete=models.SET_NULL, to=orm['services.Service']),
                      keep_default=False)

        # Adding field 'Service.lft'
        db.add_column('services_service', 'lft',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=None, db_index=True),
                      keep_default=False)

        # Adding field 'Service.rght'
        db.add_column('services_service', 'rght',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=None, db_index=True),
                      keep_default=False)

        # Adding field 'Service.tree_id'
        db.add_column('services_service', 'tree_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=None, db_index=True),
                      keep_default=False)

        # Adding field 'Service.level'
        db.add_column('services_service', 'level',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=None, db_index=True),
                      keep_default=False)

        # Deleting field 'Price.subservice'
        db.delete_column('services_price', 'subservice_id')


        # Changing field 'Price.service'
        db.alter_column('services_price', 'service_id', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['services.Service']))

    def backwards(self, orm):
        # Adding model 'SubService'
        db.create_table('services_subservice', (
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('service', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['services.Service'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('side_text', self.gf('django.db.models.fields.TextField')()),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('services', ['SubService'])

        # Deleting model 'SeviceSign'
        db.delete_table('services_sevicesign')

        # Deleting field 'Service.parent'
        db.delete_column('services_service', 'parent_id')

        # Deleting field 'Service.lft'
        db.delete_column('services_service', 'lft')

        # Deleting field 'Service.rght'
        db.delete_column('services_service', 'rght')

        # Deleting field 'Service.tree_id'
        db.delete_column('services_service', 'tree_id')

        # Deleting field 'Service.level'
        db.delete_column('services_service', 'level')

        # Adding field 'Price.subservice'
        db.add_column('services_price', 'subservice',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['services.SubService'], null=True, blank=True),
                      keep_default=False)


        # Changing field 'Price.service'
        db.alter_column('services_price', 'service_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['services.Service'], null=True))

    models = {
        'services.category': {
            'Meta': {'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'services.price': {
            'Meta': {'object_name': 'Price'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.Service']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'services.service': {
            'Meta': {'object_name': 'Service'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.SubCategory']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['services.Service']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'short_description': ('django.db.models.fields.TextField', [], {}),
            'side_text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'services.sevicesign': {
            'Meta': {'object_name': 'SeviceSign'},
            'desired_date': ('django.db.models.fields.DateField', [], {}),
            'desired_time': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initials': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.Service']"})
        },
        'services.subcategory': {
            'Meta': {'object_name': 'SubCategory'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['services.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['services']