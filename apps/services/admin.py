# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor 

from models import *

class ServiceAdminForm(forms.ModelForm):
    description = forms.CharField(widget=Redactor, label=u'Полное описание')
    side_text = forms.CharField(widget=Redactor, label=u'Текст сбоку')

class CategoryAdminForm(forms.ModelForm):
    description = forms.CharField(widget=Redactor, label=u'описание')


class PriceInline(admin.TabularInline):
    model = Price
    extra = 0

class RecomendationInline(admin.TabularInline):
    model = Recomendation
    extra = 0

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(Category, CategoryAdmin)


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)
    form = CategoryAdminForm

admin.site.register(SubCategory, SubCategoryAdmin)


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('title','parent','category','id',)
    list_filter = ('category','parent',)
    form = ServiceAdminForm
    inlines = [PriceInline, RecomendationInline]

admin.site.register(Service, ServiceAdmin)


class ServiceSignAdmin(admin.ModelAdmin):
    list_display = ('service','initials','desired_date',)

admin.site.register(ServiceSign, ServiceSignAdmin)


class MonthAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(Month, MonthAdmin)


class RecomendationTypeAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(RecomendationType, RecomendationTypeAdmin)