# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView, FormView

from models import *
from forms import *


class ServiceListView(TemplateView):
    template_name = 'services/service_list.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ServiceListView, self).get_context_data()

        category_id = self.kwargs.get('subcategory_id')
        category = SubCategory.objects.get(pk=category_id)
        service_list = Service.objects.filter(category=category_id, level=0)

        context['category'] = category
        context['service_list'] = service_list
        return context


class ServiceView(TemplateView):
    template_name = 'services/service_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ServiceView, self).get_context_data()

        service_id = self.kwargs.get('service_id')
        service = Service.objects.get(pk=service_id)

        if service.level == 0:
            service_title = service.title
        elif service.level == 1:
            service_title = service.parent.title

        context['service'] = service
        context['category'] = service.category
        context['service_title'] = service_title
        return context


class ServiceSignFormView(FormView):
    template_name = 'services/sign_form.html'
    form_class = ServiceSignForm
    success_template = 'services/sign_success.html'

    def get_context_data(self, **kwargs):
        context = super(ServiceSignFormView, self).get_context_data()

        if self.request.method == 'GET':
            service_id = self.request.GET.get('serv')
        elif self.request.method == 'POST':
            service_id = self.request.POST.get('service')

        service = Service.objects.get(pk=service_id)

        if self.request.method == 'GET':
            context['form'] = ServiceSignForm(initial={'service':service})
        elif self.request.method == 'POST':
            context['form'] = ServiceSignForm(self.request.POST)

        context['service'] = service
        return context

    def get(self, request, **kwargs):
        context = self.get_context_data()

        html_code = render_to_string(self.template_name, context, context_instance=RequestContext(request))
        return HttpResponse(html_code)

    def form_valid(self, form):
        ServiceSign.objects.create(**form.cleaned_data)
        return direct_to_template(self.request, self.success_template)


class CalendarView(TemplateView):
    template_name = 'services/calendar.html'

    def get_context_data(self, **kwargs):
        context = super(CalendarView, self).get_context_data()

        current_month_id = self.kwargs.get('months_id')
        current_month = Month.objects.get(pk=current_month_id)

        recomendations_list = current_month.recomendation_set.all()
        month_list = Month.objects.all()

        context['recomendations_list'] = recomendations_list
        context['month_list'] = month_list
        context['current_month'] = current_month
        return context