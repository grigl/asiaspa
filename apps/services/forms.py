# -*- coding: utf-8 -*-
from django import forms

from models import ServiceSign

class ServiceSignForm(forms.ModelForm):
    class Meta:
        model = ServiceSign