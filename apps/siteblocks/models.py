# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
import datetime
import os
from pytils.translit import translify
from django.db.models.signals import post_save
from apps.utils.managers import PublishedManager
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from sorl.thumbnail import ImageField

def image_path(self, instance, filename):
    filename = translify(filename).replace(' ', '_')
    return os.path.join('uploads', 'images/menu', filename)

def gallery_image_path(instance, filename):
    filename = translify(filename).replace(' ', '_')
    galleryname = translify(instance.gallery.title).replace(' ', '_')
    return os.path.join('uploads', 'images/galleries', galleryname, filename)

def slider_image_path(instance, filename):
    filename = translify(filename).replace(' ', '_')
    return os.path.join('uploads', 'images/slider', filename)

class SiteMenu(MPTTModel):
    title = models.CharField(
        max_length = 150, 
        verbose_name = u'Название'
    )
    image = models.ImageField(
        verbose_name=u'Иконка',
        upload_to = image_path, 
        blank = True,
        null = True,
    )
    parent = TreeForeignKey(
        'self',
        verbose_name = u'Родительский раздел',
        related_name = 'children',
        blank = True,
        null = True,
    )
    url = models.CharField(
        verbose_name = u'url', 
        max_length = 150, 
    )
    order = models.IntegerField(
        verbose_name = u'Порядок сортировки',
        default = 10,
        help_text = u'чем больше число, тем выше располагается элемент'
    )
    is_published = models.BooleanField(
        verbose_name=u'Опубликовано',
        default=True, 
    )

    objects = TreeManager()

    class Meta:
        verbose_name =_(u'menu_item')
        verbose_name_plural =_(u'menu_items')
        ordering = ['-order']

    class MPTTMeta:
        order_insertion_by = ['order']

    def __unicode__(self):
        return self.title

def strip_url_title(sender, instance, created, **kwargs):
    # remove the first and the last space
    instance.title = instance.title.strip()
    instance.url = instance.url.strip()
    instance.save()

post_save.connect(strip_url_title, sender=SiteMenu)

type_choices = (
    (u'input',u'input'),
    (u'textarea',u'textarea'),
    (u'redactor',u'redactor'),
)

class Settings(models.Model):
    title = models.CharField(
        verbose_name = u'Название',
        max_length = 150,
    )
    name = models.CharField( 
        verbose_name = u'Служебное имя',
        max_length = 250,
    )
    value = models.TextField(
        verbose_name = u'Значение'
    )
    type = models.CharField(
        max_length=20,
        verbose_name=u'Тип значения',
        choices=type_choices
    )
    class Meta:
        verbose_name =_(u'site_setting')
        verbose_name_plural =_(u'site_settings')

    def __unicode__(self):
        return u'%s' % self.name


class ClinicMenu(models.Model):
    title = models.CharField(
        max_length = 150, 
        verbose_name = u'Название'
    )
    url = models.CharField(
        verbose_name = u'url', 
        max_length = 150, 
    )
    order = models.IntegerField(
        verbose_name = u'Порядок сортировки',
        default = 10,
        help_text = u'чем больше число, тем выше располагается элемент'
    )
    is_published = models.BooleanField(
        verbose_name=u'Опубликовано',
        default=True, 
    )

    class Meta:
        verbose_name = u'элемент меню страницы клиники'
        verbose_name_plural = u'элементы меню страницы клиники'
        ordering = ['-order']

    def __unicode__(self):
        return self.title

    def save(self):
        self.title = self.title.strip()
        self.url = self.url.strip()
        
        super(ClinicMenu, self).save()


class Gallery(models.Model):
    gallery_type_choices = (
        (u'about', u'галлерея "О клинике"'),
    )

    title = models.CharField(verbose_name=u'название', max_length=150)
    is_published = models.BooleanField(verbose_name=u'опубликовано', default=True)
    gallery_type = models.CharField(verbose_name=u'тип галлереи', max_length=150, choices=gallery_type_choices)

    objects = PublishedManager()

    class Meta:
        verbose_name = u'фотогаллерея'
        verbose_name_plural = u'фотогаллереи'

    def __unicode__(self):
        return self.title


class GalleryImage(models.Model):
    gallery = models.ForeignKey(Gallery, verbose_name=u'галлерея')
    image = ImageField(verbose_name=u'картинка', upload_to=gallery_image_path)
    show_on_index = models.BooleanField(verbose_name=u'показывать на главной', default=False)

    class Meta:
        verbose_name = u'фотография'
        verbose_name_plural = u'фотографии'

    def __unicode__(self):
        return self.image.name


class SliderItem(models.Model):
    slot_choices = (
        (u'big', u'большой баннер'),
        (u'small_top', u'маленький баннер сверху'),
        (u'small_bottom', u'маленький баннер снизу'),
        (u'medium_top', u'средний баннер сверху'),
        (u'medium_bottom', u'средний баннер снизу'),
    )

    image = ImageField(verbose_name=u'картинка', upload_to=slider_image_path)
    slot = models.CharField(verbose_name=u'место расположения', max_length=150, choices=slot_choices)
    is_published = models.BooleanField(verbose_name=u'опубликован', default=True)
    title = models.CharField(verbose_name=u'подпись', max_length=150, blank=True)
    link = models.URLField(verbose_name=u'ссылка', blank=True)

    crop_size = [260, 306]

    class Meta:
        verbose_name = u'элемент слайдера'
        verbose_name_plural = u'элементы слайдера'

    def __unicode__(self):
        return self.image.name

    def has_text(self):
        if self.title == None or self.title == '':
            return False
        else: 
            return True


