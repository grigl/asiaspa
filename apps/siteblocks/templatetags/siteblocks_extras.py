# -*- coding: utf-8 -*-
from django import template
from django.utils.safestring import mark_safe

from apps.siteblocks.models import *
from apps.services.models import Category
from apps.specs.models import Spec
from apps.reviews.models import Review
from apps.articles.models import NewsItem

register = template.Library()

def get_active_menu(url, site_menu):
    
    request_path = url
    
    for i, menu_item in enumerate(site_menu):
        setattr(site_menu[i], 'is_active', False)
        #if request_path.startswith(menu_item.path):
            #site_menu[i].is_active = True


    for i, menu_item in enumerate(site_menu):
        setattr(site_menu[i], 'is_active', False)
        if not menu_item.parent:
            for menu_subitem in site_menu:
                # highlight parent menu item
                if menu_subitem.parent == menu_item and menu_subitem.is_active:
                    site_menu[i].is_active = True
        elif menu_item.is_active:
            for menu_subitem in site_menu:
                # remove redundant sibling highlight
                if menu_subitem.parent \
                     and menu_subitem.parent.path == menu_item.parent.path \
                     and menu_subitem.path.startswith(menu_item.path) \
                     and len(menu_subitem.path) > len(menu_item.path) \
                     and menu_subitem.is_active:
                        site_menu[i].is_active = False
    return site_menu

@register.inclusion_tag("siteblocks/block_menu.html")
def block_menu(url):
    url = url.split('/')

    if url[1]:
        current = u'/%s/' % url[1]
    else:
        current = u'/'
    menu = SiteMenu.objects.all()
    menu = get_active_menu(url, menu)
    return {'menu': menu, 'current': current}

@register.inclusion_tag("siteblocks/block_setting.html")
def block_static(name):
    try:
        setting = Settings.objects.get(name = name)
    except Settings.DoesNotExist:
        setting = False
    return {'block': block,}


@register.inclusion_tag("siteblocks/side_menu.html")
def get_side_menu(path, current_category=None):
    category_list = Category.objects.all()

    if current_category:
        current_parent_category = current_category.category
    else:
        current_parent_category = None

    if '/calendar/' in path:
        current_category = 'calendar'

    return { 'category_list':category_list, 
             'path': path, 'current_category': current_category, 
             'current_parent_category': current_parent_category }


@register.inclusion_tag("siteblocks/clinic_menu.html")
def get_clinic_menu(path):
    menu_items = ClinicMenu.objects.all()
    current_item = None

    for item in menu_items:
        if item.url in path:
            current_item = item

    return { 'menu_item_list': menu_items, 'current_item': current_item }


@register.inclusion_tag("siteblocks/side_random_spec.html")
def get_side_random_spec(service=False):
    if service and service.has_specs:
        spec = service.specs.all().order_by('?')[0]
    else:
        spec = Spec.objects.order_by('?')[0]

    return { 'spec': spec }


@register.inclusion_tag("siteblocks/side_random_review.html")
def get_side_random_review():
    review = Review.objects.filter(is_published=True).order_by('?')[0]

    return { 'review': review }


@register.inclusion_tag("siteblocks/gallery.html")
def get_about_gallery():
    try:
        gallery = Gallery.objects.get(gallery_type='about')
        photo_list = gallery.galleryimage_set.all()
    except: 
        photo_list = None

    return { 'photo_list': photo_list }


@register.inclusion_tag("siteblocks/gallery.html")
def get_index_gallery():
    try:
        gallery = Gallery.objects.get(gallery_type='about')
        photo_list = gallery.galleryimage_set.filter(show_on_index=True)
    except: 
        photo_list = None

    return { 'photo_list': photo_list }


@register.inclusion_tag("siteblocks/index_last_news.html")
def get_index_last_news():
    try:
        last_news = NewsItem.objects.filter(show_on_index=True)[:3]
    except: 
        last_news = None

    return { 'news_list': last_news }


@register.inclusion_tag("siteblocks/index_slider.html")
def get_index_slider():
    slider_items_big = SliderItem.objects.filter(slot='big')
    slider_items_small_top = SliderItem.objects.filter(slot='small_top')
    slider_items_small_bottom = SliderItem.objects.filter(slot='small_bottom')
    slider_items_medium_top = SliderItem.objects.filter(slot='medium_top')
    slider_items_medium_bottom = SliderItem.objects.filter(slot='medium_bottom')

    return { 'slider_items_big': slider_items_big, 
             'slider_items_small_top': slider_items_small_top,
             'slider_items_small_bottom': slider_items_small_bottom,
             'slider_items_medium_top': slider_items_medium_top,
             'slider_items_medium_bottom': slider_items_medium_bottom }


@register.simple_tag
def get_setting(name):
    try:
        setting_value = Settings.objects.get(name=name).value
    except:
        setting_value = None

    return mark_safe(setting_value)
