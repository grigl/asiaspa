# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.siteblocks.models import *
from apps.utils.widgets import RedactorMini
from sorl.thumbnail.admin import AdminImageMixin
from mptt.admin import MPTTModelAdmin
from apps.utils.widgets import Redactor, AdminImageCrop



class SiteMenuAdmin(AdminImageMixin, MPTTModelAdmin):
    list_display = ('title', 'url', 'order', 'is_published',)
    list_display_links = ('title', 'url',)
    list_editable = ('order', 'is_published',)

admin.site.register(SiteMenu, SiteMenuAdmin)

class ClinicMenuAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'order', 'is_published',)
    list_display_links = ('title', 'url',)
    list_editable = ('order', 'is_published',)

admin.site.register(ClinicMenu, ClinicMenuAdmin)

#--Виджеты jquery Редактора
class SettingsAdminForm(forms.ModelForm):
    class Meta:
        model = Settings

    def __init__(self, *args, **kwargs):
        super(SettingsAdminForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs['instance']
        except KeyError:
            instance = False
        if instance:
            if instance.type == u'input':
                self.fields['value'].widget = forms.TextInput()
            elif instance.type == u'textarea':
                self.fields['value'].widget = forms.Textarea()
            elif instance.type == u'redactor':
                self.fields['value'].widget = Redactor(attrs={'cols': 100, 'rows': 10},)

#--Виджеты jquery Редактора

class SettingsAdmin(admin.ModelAdmin):
    list_display = ('title','name','value',)
    form = SettingsAdminForm
admin.site.register(Settings, SettingsAdmin)


class GalleryImageInline(admin.TabularInline):
    model = GalleryImage
    extra = 0 

class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_published',)
    list_editable = ('is_published',)
    inlines = [GalleryImageInline]

admin.site.register(Gallery, GalleryAdmin)


class SliderItemForm(forms.ModelForm):
    image = forms.ImageField(
        widget=AdminImageCrop(attrs={'path': 'siteblocks/slideritem'}),
        label=u'Изображение',
        required=False
    )

    class Meta:
        model = SliderItem


class SliderItemAdmin(admin.ModelAdmin):
    list_display = ('slot', 'id', 'title', 'link', 'is_published')
    list_editable = ('is_published',)
    list_filter = ('slot',)
    form = SliderItemForm

admin.site.register(SliderItem, SliderItemAdmin)

