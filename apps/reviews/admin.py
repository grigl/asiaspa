# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor 

from models import *


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('initials','created_at','is_published',)
    list_filter = ('is_published',)
    readonly_fields = ('created_at',)

admin.site.register(Review, ReviewAdmin)