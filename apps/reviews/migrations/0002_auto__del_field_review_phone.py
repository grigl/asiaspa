# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Review.phone'
        db.delete_column('reviews_review', 'phone')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Review.phone'
        raise RuntimeError("Cannot reverse this migration. 'Review.phone' and its values cannot be restored.")

    models = {
        'reviews.review': {
            'Meta': {'object_name': 'Review'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 8, 1, 0, 0)'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initials': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'review': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['reviews']