# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url
from django.http import HttpResponseRedirect

from views import index
from apps.specs.views import *
from apps.reviews.views import *
from apps.services.views import *
from apps.articles.views import *

from apps.services.models import Month

urlpatterns = patterns('',
    url(r'^$', index, name='index'),
    url(r'^faq/', include('apps.faq.urls')),
    url(r'^cart/', include('apps.orders.urls')),
    url(r'^cabinet/', include('apps.inheritanceUser.urls')),

    url(r'^clinic/$', lambda x: HttpResponseRedirect('/clinic/about/'), name='clinic_url'),

    url(r'^clinic/news/$', NewsListView.as_view(), name='news_url'),
    url(r'^clinic/news/(?P<pk>\d+)/$', NewsDetailView.as_view(), name='news_item_url'),

    url(r'^clinic/social/$', SocialListView.as_view(), name='social_url'),
    url(r'^clinic/social/(?P<pk>\d+)/$', SocialDetailView.as_view(), name='social_item_url'),

    url(r'^clinic/licence/$', LicenceView.as_view(), name='licence_url'),

    url(r'^specs/$', SpecListView.as_view(), name='spec_list_url'),
    url(r'^specs/question_form/$', SpecQFormView.as_view(), name='spec_q_form_url'),

    url(r'^reviews/$', ReviewListView.as_view(), name='review_list_url'),
    url(r'^reviews/review_form/$', ReviewFormView.as_view(), name="review_form_url"),

    url(r'^services/(?P<subcategory_id>\d+)/$', ServiceListView.as_view(), name='service_list_url'),
    url(r'^services/(?P<subcategory_id>\d+)/(?P<service_id>\d+)/$', ServiceView.as_view(), name='service_url'),
    url(r'^services/sign_form/$', ServiceSignFormView.as_view(), name="service_sign_form_url"),

    url(r'^calendar/$', lambda x: HttpResponseRedirect('/calendar/%d/' % Month.get_current_month()), name='calendar_url'),
    url(r'^calendar/(?P<months_id>\d+)/$', CalendarView.as_view(), name='calendar_month_url'),
)