# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Spec'
        db.create_table('specs_spec', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('initials', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('title', self.gf('django.db.models.fields.TextField')()),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('short_desc', self.gf('django.db.models.fields.TextField')()),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('specs', ['Spec'])

        # Adding model 'Question'
        db.create_table('specs_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('spec', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['specs.Spec'])),
            ('question', self.gf('django.db.models.fields.TextField')()),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal('specs', ['Question'])


    def backwards(self, orm):
        # Deleting model 'Spec'
        db.delete_table('specs_spec')

        # Deleting model 'Question'
        db.delete_table('specs_question')


    models = {
        'specs.question': {
            'Meta': {'object_name': 'Question'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.TextField', [], {}),
            'spec': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['specs.Spec']"})
        },
        'specs.spec': {
            'Meta': {'object_name': 'Spec'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'initials': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'short_desc': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['specs']