# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Question.answer'
        db.add_column('specs_question', 'answer',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'Question.send_answer'
        db.add_column('specs_question', 'send_answer',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Question.answer'
        db.delete_column('specs_question', 'answer')

        # Deleting field 'Question.send_answer'
        db.delete_column('specs_question', 'send_answer')


    models = {
        'specs.question': {
            'Meta': {'object_name': 'Question'},
            'answer': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.TextField', [], {}),
            'send_answer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'spec': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['specs.Spec']"})
        },
        'specs.spec': {
            'Meta': {'object_name': 'Spec'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'initials': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'short_desc': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['specs']