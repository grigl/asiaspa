# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Spec.title'
        db.alter_column('specs_spec', 'title', self.gf('django.db.models.fields.CharField')(max_length=150))

    def backwards(self, orm):

        # Changing field 'Spec.title'
        db.alter_column('specs_spec', 'title', self.gf('django.db.models.fields.TextField')())

    models = {
        'specs.question': {
            'Meta': {'object_name': 'Question'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.TextField', [], {}),
            'spec': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['specs.Spec']"})
        },
        'specs.spec': {
            'Meta': {'object_name': 'Spec'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'initials': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'short_desc': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['specs']