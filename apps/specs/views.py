# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template

from django.views.generic import ListView, DetailView, TemplateView, FormView

from models import *
from forms import *


class SpecListView(ListView):
    template_name = '/specs/spec_list.html'
    model = Spec
    paginate_by = 7


class SpecQFormView(FormView):
    form_class = SpecQForm
    template_name = 'specs/q_form.html'
    success_template = 'specs/spec_q_success.html'

    def get(self, request, **kwargs):
        context = self.get_context_data()

        spec_id = request.GET.get('spec')
        if spec_id:
            spec = Spec.objects.get(pk=spec_id)
        else:
            spec = None

        context['form'] = SpecQForm(initial={'spec':spec})

        html_code = render_to_string(self.template_name, context, context_instance=RequestContext(request))
        return HttpResponse(html_code)

    def form_valid(self, form):
        Question.objects.create(**form.cleaned_data)
        return direct_to_template(self.request, self.success_template)