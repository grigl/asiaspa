# -*- coding: utf-8 -*-
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from apps.siteblocks.models import Settings
from settings import DEFAULT_FROM_EMAIL as from_email

def send_new_question_email(instance):
    template_name = 'specs/new_question_email.html'

    subject = 'новый вопрос специалисту'
    message = render_to_string(template_name, {'object':instance})
    to_email = Settings.objects.get(name='main_email').value

    msg = EmailMessage(subject, message, from_email, [to_email])
    msg.content_subtype = "html"
    msg.send()


def send_answer_email(instance):
    template_name = 'specs/spec_answer_email.html'

    subject = 'ответ на ваш вопрос'
    message = render_to_string(template_name, {'object':instance})
    to_email = instance.email

    msg = EmailMessage(subject, message, from_email, [to_email])
    msg.content_subtype = "html"
    msg.send()