# -*- coding: utf-8 -*-
import os, datetime
from django.db import models
from django.contrib.auth.models import User

from pytils.translit import translify
from django.core.urlresolvers import reverse

from sorl.thumbnail import ImageField

from apps.siteblocks.models import Settings

from mailers import *


def image_file_path(instance, filename):
    return os.path.join('images', instance.__class__.__name__.lower(), translify(filename).replace(' ', '_') )

class Spec(models.Model):
    initials = models.CharField(verbose_name=u'инициалы', max_length=150)
    title = models.CharField(verbose_name=u'подпись', max_length=150)
    image = ImageField(verbose_name=u'фотография', upload_to=image_file_path)
    short_desc = models.TextField(verbose_name=u'краткое описание')
    description = models.TextField(verbose_name=u'полное описание')

    class Meta:
        verbose_name = u'специалист'
        verbose_name_plural = u'специалисты'

    def __unicode__(self):
        return self.initials


class Question(models.Model):
    spec = models.ForeignKey(Spec, verbose_name=u'ответчик')
    question = models.TextField(verbose_name=u'текст вопроса')
    email = models.EmailField(verbose_name=u'электропочта автора вопроса')
    answer = models.TextField(verbose_name=u'ответ', blank=True)
    send_answer = models.BooleanField(verbose_name=u'отправить ответ', default=False, blank=True,
                                      help_text=u'если галка активна, при сохранении ответ уйдет на адрес автора вопроса')

    class Meta:
        verbose_name = u'вопрос'
        verbose_name_plural = u'вопросы'

    def __unicode__(self):
        return u'вопрос от' + self.email

    def save(self, **kwargs):
        if self.pk is None:
            send_new_question_email(self)

        super(Question, self).save()

        if self.send_answer:
            if self.answer is not None or self.answer is not '':
                send_answer_email(self)

    