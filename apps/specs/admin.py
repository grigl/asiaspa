# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.utils.widgets import Redactor 

from models import *

class SpecAdminForm(forms.ModelForm):
    description = forms.CharField(widget=Redactor, label=u'текст')

    class Meta:
        model = Spec

class SpecAdmin(admin.ModelAdmin):
    list_display = ('initials','title')
    form = SpecAdminForm

admin.site.register(Spec, SpecAdmin)


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('email','spec')

admin.site.register(Question, QuestionAdmin)