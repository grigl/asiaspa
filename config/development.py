# -*- coding: utf-8 -*-

from settings import DATABASE_NAME
from dbpass import DATABASE_PASSWORD
DEBUG = True
THUMBNAIL_DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': DATABASE_NAME,
        'USER': 'root',
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': '',
        'PORT': '',
    }
}
EMAIL_PORT = 25
TIME_ZONE = 'Europe/Moscow'
#TIME_ZONE = 'Asia/Yakutsk'
