-- MySQL dump 10.13  Distrib 5.1.62, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: asiaspa
-- ------------------------------------------------------
-- Server version	5.1.62-0ubuntu0.11.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tools_dashboard_prefer_dashboard_id_374bce90a8a4eefc_uniq` (`dashboard_id`,`user_id`),
  KEY `admin_tools_dashboard_preferences_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_2faedda1f8487376` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{}','dashboard'),(2,1,'{}','specs-dashboard'),(3,1,'{}','services-dashboard'),(4,1,'{}','siteblocks-dashboard'),(5,1,'{}','articles-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_6af2836063b2844f` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_newsitem`
--

DROP TABLE IF EXISTS `articles_newsitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_newsitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `short_text` longtext NOT NULL,
  `full_text` longtext NOT NULL,
  `show_on_index` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_newsitem`
--

LOCK TABLES `articles_newsitem` WRITE;
/*!40000 ALTER TABLE `articles_newsitem` DISABLE KEYS */;
INSERT INTO `articles_newsitem` VALUES (1,'Новое направление','В клинике эстетической косметологии открылось новое направление Гинекология','<p>В клинике эстетической косметологии открылось новое направление Гинекология<br></p>',1,'2013-08-13'),(2,'Омоложение E-Matrix','Новая услуга, альтернатива лазерной шлифовки','<p>Новая услуга, альтернатива лазерной шлифовки<br></p><p>Новая услуга, альтернатива лазерной шлифовки<br></p><p>Новая услуга, альтернатива лазерной шлифовки<br></p>',1,'2013-08-13'),(3,'Vela Smooth','Абсолютный хит эстетической медицины по специальной цене','<p>Абсолютный хит эстетической медицины по специальной цене<br></p><p>Абсолютный хит эстетической медицины по специальной цене<br></p><p>Абсолютный хит эстетической медицины по специальной цене<br></p>',1,'2013-08-13');
/*!40000 ALTER TABLE `articles_newsitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_social`
--

DROP TABLE IF EXISTS `articles_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  `full_text` longtext NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_social`
--

LOCK TABLES `articles_social` WRITE;
/*!40000 ALTER TABLE `articles_social` DISABLE KEYS */;
INSERT INTO `articles_social` VALUES (1,'Участвовали в подготовке Мисс Забайкалья','images/social/preview-sample2.png','<p><p></p><p>«Asia-Spa» - это оазис красоты и здоровья, в который можно войти, захлопнуть за собой дверь, оставив снаружи все свои проблемы. Cледуя философским традициям Востока и современности Запада, в самом центре ммЧиты 20 декабря 2006 года открылся салон «Asia-Spa». Эксклюзивность салона в новизне и качестве услуг. Современный интерьер дополняется целостной концепцией терапии по уходу за лицом и телом.</p><p><p>«Asia-Spa» - это оазис красоты и здоровья, в который можно войти, захлопнуть за собой дверь, оставив снаружи все свои проблемы. Cледуя философским традициям Востока и современности Запада, в самом центре ммЧиты 20 декабря 2006 года открылся салон «Asia-Spa». Эксклюзивность салона в новизне и качестве услуг. Современный интерьер дополняется целостной концепцией терапии по уходу за лицом и телом.</p></p><br></p>','2013-08-13'),(2,'Фитнесс-вечеринки','images/social/preview-sample2_1.png','<p>Своим клиентам «Asia-Spa» предлагает широчайший ассортимент косметологического оборудования, который позволяет разрабатывать индивидуальные программы для каждого клиента. Это могут быть как отдельные курсы процедур, так и комплексные эстетические программы для лица и тела, решающие проблемы, возникающие в любом возрасте.<br></p><p>Своим клиентам «Asia-Spa» предлагает широчайший ассортимент косметологического оборудования, который позволяет разрабатывать индивидуальные программы для каждого клиента. Это могут быть как отдельные курсы процедур, так и комплексные эстетические программы для лица и тела, решающие проблемы, возникающие в любом возрасте.<br></p>','2013-08-13');
/*!40000 ALTER TABLE `articles_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add log entry',7,'add_logentry'),(20,'Can change log entry',7,'change_logentry'),(21,'Can delete log entry',7,'delete_logentry'),(22,'Can add kv store',8,'add_kvstore'),(23,'Can change kv store',8,'change_kvstore'),(24,'Can delete kv store',8,'delete_kvstore'),(25,'Can add migration history',9,'add_migrationhistory'),(26,'Can change migration history',9,'change_migrationhistory'),(27,'Can delete migration history',9,'delete_migrationhistory'),(28,'Can add bookmark',10,'add_bookmark'),(29,'Can change bookmark',10,'change_bookmark'),(30,'Can delete bookmark',10,'delete_bookmark'),(31,'Can add dashboard preferences',11,'add_dashboardpreferences'),(32,'Can change dashboard preferences',11,'change_dashboardpreferences'),(33,'Can delete dashboard preferences',11,'delete_dashboardpreferences'),(34,'Can add menu_item',12,'add_sitemenu'),(35,'Can change menu_item',12,'change_sitemenu'),(36,'Can delete menu_item',12,'delete_sitemenu'),(37,'Can add site_setting',13,'add_settings'),(38,'Can change site_setting',13,'change_settings'),(39,'Can delete site_setting',13,'delete_settings'),(40,'Can add page_item',14,'add_page'),(41,'Can change page_item',14,'change_page'),(42,'Can delete page_item',14,'delete_page'),(43,'Can add файл',15,'add_pagedoc'),(44,'Can change файл',15,'change_pagedoc'),(45,'Can delete файл',15,'delete_pagedoc'),(46,'Can add картинка',16,'add_pagepic'),(47,'Can change картинка',16,'change_pagepic'),(48,'Can delete картинка',16,'delete_pagepic'),(49,'Can add meta',17,'add_metadata'),(50,'Can change meta',17,'change_metadata'),(51,'Can delete meta',17,'delete_metadata'),(52,'Can add news_category',18,'add_newscategory'),(53,'Can change news_category',18,'change_newscategory'),(54,'Can delete news_category',18,'delete_newscategory'),(55,'Can add news_item',19,'add_news'),(56,'Can change news_item',19,'change_news'),(57,'Can delete news_item',19,'delete_news'),(58,'Can add специалист',20,'add_spec'),(59,'Can change специалист',20,'change_spec'),(60,'Can delete специалист',20,'delete_spec'),(61,'Can add вопрос',21,'add_question'),(62,'Can change вопрос',21,'change_question'),(63,'Can delete вопрос',21,'delete_question'),(64,'Can add отзыв',22,'add_review'),(65,'Can change отзыв',22,'change_review'),(66,'Can delete отзыв',22,'delete_review'),(67,'Can add раздел',23,'add_category'),(68,'Can change раздел',23,'change_category'),(69,'Can delete раздел',23,'delete_category'),(70,'Can add подраздел',24,'add_subcategory'),(71,'Can change подраздел',24,'change_subcategory'),(72,'Can delete подраздел',24,'delete_subcategory'),(73,'Can add услуга',25,'add_service'),(74,'Can change услуга',25,'change_service'),(75,'Can delete услуга',25,'delete_service'),(82,'Can add цена услуги',28,'add_price'),(83,'Can change цена услуги',28,'change_price'),(84,'Can delete цена услуги',28,'delete_price'),(85,'Can add элемент меню страницы клиники',29,'add_clinicmenu'),(86,'Can change элемент меню страницы клиники',29,'change_clinicmenu'),(87,'Can delete элемент меню страницы клиники',29,'delete_clinicmenu'),(88,'Can add фотогаллерея',30,'add_gallery'),(89,'Can change фотогаллерея',30,'change_gallery'),(90,'Can delete фотогаллерея',30,'delete_gallery'),(91,'Can add фотография',31,'add_galleryimage'),(92,'Can change фотография',31,'change_galleryimage'),(93,'Can delete фотография',31,'delete_galleryimage'),(94,'Can add запись на процедуру',32,'add_servicesign'),(95,'Can change запись на процедуру',32,'change_servicesign'),(96,'Can delete запись на процедуру',32,'delete_servicesign'),(97,'Can add ',33,'add_social'),(98,'Can change ',33,'change_social'),(99,'Can delete ',33,'delete_social'),(100,'Can add ',34,'add_newsitem'),(101,'Can change ',34,'change_newsitem'),(102,'Can delete ',34,'delete_newsitem'),(103,'Can add элемент слайдера',35,'add_slideritem'),(104,'Can change элемент слайдера',35,'change_slideritem'),(105,'Can delete элемент слайдера',35,'delete_slideritem');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'grigl','','','grigl@mail.ru','pbkdf2_sha256$10000$Q2ZD49Fv3HMK$Gx3/dkIUzBO7lKbdQJpEtaXKlccT43Wjo0vkzPCLBhQ=',1,1,1,'2013-08-13 19:57:05','2013-08-02 10:49:00');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`),
  CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2013-08-02 11:27:29',1,20,'1','Дугарова Дарима Гатыповна',1,''),(2,'2013-08-02 11:50:13',1,20,'1','Дугарова Дарима Гатыповна',2,'Изменен description.'),(3,'2013-08-02 11:50:26',1,20,'1','Дугарова Дарима Гатыповна',2,'Изменен description.'),(4,'2013-08-02 11:50:44',1,20,'1','Дугарова Дарима Гатыповна',2,'Изменен description.'),(5,'2013-08-02 15:03:32',1,23,'1','Эстетическая косметология',1,''),(6,'2013-08-02 15:09:36',1,24,'1','Терапевтическая косметология',1,''),(7,'2013-08-02 15:10:10',1,24,'1','Терапевтическая косметология',3,''),(8,'2013-08-02 15:11:18',1,24,'2','Терапевтическая косметология',1,''),(9,'2013-08-02 15:11:46',1,24,'2','Терапевтическая косметология',2,'Ни одно поле не изменено.'),(10,'2013-08-02 15:11:58',1,24,'2','Терапевтическая косметология',2,'Ни одно поле не изменено.'),(11,'2013-08-02 15:12:35',1,24,'2','Терапевтическая косметология',2,'Изменен slug.'),(12,'2013-08-02 15:13:28',1,24,'3','Аппаратная косметология',1,''),(13,'2013-08-02 15:20:56',1,24,'4','Уходовые процедуры',1,''),(14,'2013-08-02 15:24:44',1,24,'5','Декоративная косметология',1,''),(15,'2013-08-02 15:27:21',1,23,'1','Эстетическая косметология',2,'Ни одно поле не изменено.'),(16,'2013-08-02 18:19:26',1,24,'2','Терапевтическая косметология',2,'Изменен description.'),(17,'2013-08-02 18:32:07',1,25,'1','Трихология',1,''),(18,'2013-08-02 18:33:05',1,25,'2','Дерматоонкология',1,''),(22,'2013-08-13 12:37:41',1,25,'3','Диагностика',1,''),(23,'2013-08-13 12:39:05',1,25,'4','Радиоволновое удаление',1,''),(24,'2013-08-13 12:43:16',1,13,'1','about_text',1,''),(25,'2013-08-13 12:43:23',1,13,'1','about_text',2,'Изменен value.'),(26,'2013-08-13 12:43:35',1,13,'1','about_text',2,'Изменен value.'),(27,'2013-08-13 14:41:01',1,35,'1','uploads/images/slider/slider-big-sample.png',1,''),(28,'2013-08-13 14:41:27',1,35,'2','uploads/images/slider/slider-small-sample.png',1,''),(29,'2013-08-13 14:41:35',1,35,'3','uploads/images/slider/slider-small-sample_1.png',1,''),(30,'2013-08-13 14:41:47',1,35,'4','uploads/images/slider/slider-medium-sample.png',1,''),(31,'2013-08-13 14:42:16',1,35,'5','uploads/images/slider/slider-medium-sample_1.png',1,''),(32,'2013-08-13 17:44:31',1,30,'1','Галлерея страницы \"О клинике\"',2,'Добавлен фотография \"uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample.png\". Добавлен фотография \"uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_1.png\". Добавлен фотография \"uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_2.png\". Добавлен фотография \"uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_3.png\". Добавлен фотография \"uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_4.png\". Добавлен фотография \"uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_5.png\".'),(33,'2013-08-13 17:47:09',1,13,'2','map',1,''),(34,'2013-08-13 17:48:02',1,13,'2','map',2,'Изменен value.'),(35,'2013-08-13 18:18:48',1,34,'1','Новое направление',1,''),(36,'2013-08-13 18:24:54',1,34,'2','Омоложение E-Matrix',1,''),(37,'2013-08-13 18:25:29',1,34,'3','Vela Smooth',1,''),(38,'2013-08-13 18:33:39',1,34,'2','Омоложение E-Matrix',2,'Изменен show_on_index.'),(39,'2013-08-13 18:35:09',1,29,'1','Миссия и Политика',1,''),(40,'2013-08-13 18:35:42',1,29,'1','Миссия и Политика',2,'Изменен url.'),(41,'2013-08-13 18:36:34',1,29,'2','Общественная жизнь',1,''),(42,'2013-08-13 18:37:13',1,29,'3','Новости',1,''),(43,'2013-08-13 18:37:31',1,29,'4','Вакансии',1,''),(44,'2013-08-13 18:37:52',1,29,'5','Лицензии и сертификаты',1,''),(45,'2013-08-13 20:01:23',1,20,'1','Дугарова Дарима Гатыповна',2,'Изменен image.'),(46,'2013-08-13 20:04:21',1,20,'2','Фамилия Имя Отчество',1,''),(47,'2013-08-13 20:05:35',1,20,'3','Фамилия Имя Отчество',1,''),(48,'2013-08-13 20:06:08',1,20,'4','Фамилия Имя Отчество',1,''),(49,'2013-08-13 20:07:40',1,23,'2','Лечебно-восстановительная медицина',1,''),(50,'2013-08-13 20:07:57',1,23,'2','Лечебно-восстановительная медицина',2,'Ни одно поле не изменено.'),(51,'2013-08-13 20:08:04',1,23,'3','Эстетическая гинекология',1,''),(52,'2013-08-13 20:08:10',1,23,'4','SPA-услуги',1,''),(53,'2013-08-13 20:09:19',1,24,'6','Дерматология',1,''),(54,'2013-08-13 20:09:39',1,24,'6','Дерматология',2,'Изменен description.'),(55,'2013-08-13 20:10:01',1,24,'7','Массаж',1,''),(56,'2013-08-13 20:10:39',1,24,'8','Мануальная терапия',1,''),(57,'2013-08-13 20:11:04',1,24,'9','Гинекология',1,''),(58,'2013-08-13 20:11:41',1,24,'10','Antiage гинекология',1,''),(59,'2013-08-13 20:11:57',1,24,'11','Интимная контурная пластика',1,''),(60,'2013-08-13 20:12:19',1,24,'12','Хирургическая коррекция малых половых губ',1,''),(61,'2013-08-13 20:12:38',1,24,'13','Диагностика и лечение недержания мочи',1,''),(62,'2013-08-13 20:12:54',1,24,'14','Обертывание',1,''),(63,'2013-08-13 20:13:02',1,24,'15','Массажи',1,''),(64,'2013-08-13 20:13:16',1,24,'16','SPA-программы',1,''),(65,'2013-08-13 20:15:55',1,25,'1','Трихология',2,'Изменен description и side_text.'),(66,'2013-08-13 20:20:33',1,25,'1','Трихология',2,'Изменен description.'),(67,'2013-08-14 11:02:34',1,33,'1','Участвовали в подготовке Мисс Забайкалья',1,''),(68,'2013-08-14 11:03:11',1,33,'2','Фитнесс-вечеринки',1,''),(69,'2013-08-14 13:36:32',1,14,'1','Вакансии (/clinic/jobs/)',1,''),(70,'2013-08-14 13:36:52',1,14,'2','Лицензии и сертификаты (/clinic/licence/)',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'log entry','admin','logentry'),(8,'kv store','thumbnail','kvstore'),(9,'migration history','south','migrationhistory'),(10,'bookmark','menu','bookmark'),(11,'dashboard preferences','dashboard','dashboardpreferences'),(12,'menu_item','siteblocks','sitemenu'),(13,'site_setting','siteblocks','settings'),(14,'page_item','pages','page'),(15,'файл','pages','pagedoc'),(16,'картинка','pages','pagepic'),(17,'meta','pages','metadata'),(18,'news_category','newsboard','newscategory'),(19,'news_item','newsboard','news'),(20,'специалист','specs','spec'),(21,'вопрос','specs','question'),(22,'отзыв','reviews','review'),(23,'раздел','services','category'),(24,'подраздел','services','subcategory'),(25,'услуга','services','service'),(28,'цена услуги','services','price'),(29,'элемент меню страницы клиники','siteblocks','clinicmenu'),(30,'фотогаллерея','siteblocks','gallery'),(31,'фотография','siteblocks','galleryimage'),(32,'запись на процедуру','services','servicesign'),(33,'','articles','social'),(34,'','articles','newsitem'),(35,'элемент слайдера','siteblocks','slideritem');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('3aec7a531d03dacf3a5109c7b510ab1e','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-08-27 12:33:55'),('3c6293c023200b26e455097e46c114ee','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-08-27 15:23:21'),('40dfd27fff8901eeb8598e64a68170ae','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-08-27 19:57:05'),('ca861e83752061053afe2ed25d24cf3a','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-08-16 11:11:19');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_news`
--

DROP TABLE IF EXISTS `newsboard_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  `short_text` longtext NOT NULL,
  `text` longtext NOT NULL,
  `on_main_page` tinyint(1) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `newsboard_news_42dc49bc` (`category_id`),
  CONSTRAINT `category_id_refs_id_4d334043d0f945e1` FOREIGN KEY (`category_id`) REFERENCES `newsboard_newscategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_news`
--

LOCK TABLES `newsboard_news` WRITE;
/*!40000 ALTER TABLE `newsboard_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsboard_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_newscategory`
--

DROP TABLE IF EXISTS `newsboard_newscategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_newscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_newscategory`
--

LOCK TABLES `newsboard_newscategory` WRITE;
/*!40000 ALTER TABLE `newsboard_newscategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsboard_newscategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_metadata`
--

DROP TABLE IF EXISTS `pages_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_metadata`
--

LOCK TABLES `pages_metadata` WRITE;
/*!40000 ALTER TABLE `pages_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_page`
--

DROP TABLE IF EXISTS `pages_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  `title` varchar(120) NOT NULL,
  `url` varchar(200) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `is_at_menu` tinyint(1) NOT NULL,
  `is_at_footer_menu` tinyint(1) NOT NULL,
  `template` varchar(100) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `pages_page_63f17a16` (`parent_id`),
  KEY `pages_page_42b06ff6` (`lft`),
  KEY `pages_page_91543e5a` (`rght`),
  KEY `pages_page_efd07f28` (`tree_id`),
  KEY `pages_page_2a8f42e8` (`level`),
  CONSTRAINT `parent_id_refs_id_3eab4d20353c0eed` FOREIGN KEY (`parent_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_page`
--

LOCK TABLES `pages_page` WRITE;
/*!40000 ALTER TABLE `pages_page` DISABLE KEYS */;
INSERT INTO `pages_page` VALUES (1,'2013-08-14 13:36:32','Вакансии','/clinic/jobs/',NULL,'<p>Вакансии<br></p>',10,1,0,0,'default.html',1,2,1,0),(2,'2013-08-14 13:36:52','Лицензии и сертификаты','/clinic/licence/',NULL,'<p>Лицензии и сертификаты<br></p>',10,1,0,0,'default.html',1,2,2,0);
/*!40000 ALTER TABLE `pages_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagedoc`
--

DROP TABLE IF EXISTS `pages_pagedoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagedoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagedoc_32d04bc7` (`page_id`),
  CONSTRAINT `page_id_refs_id_c5e8c32df41e052` FOREIGN KEY (`page_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagedoc`
--

LOCK TABLES `pages_pagedoc` WRITE;
/*!40000 ALTER TABLE `pages_pagedoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagedoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagepic`
--

DROP TABLE IF EXISTS `pages_pagepic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagepic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagepic_32d04bc7` (`page_id`),
  CONSTRAINT `page_id_refs_id_4711677842aca3ec` FOREIGN KEY (`page_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagepic`
--

LOCK TABLES `pages_pagepic` WRITE;
/*!40000 ALTER TABLE `pages_pagepic` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagepic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews_review`
--

DROP TABLE IF EXISTS `reviews_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initials` varchar(150) NOT NULL,
  `review` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `email` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews_review`
--

LOCK TABLES `reviews_review` WRITE;
/*!40000 ALTER TABLE `reviews_review` DISABLE KEYS */;
INSERT INTO `reviews_review` VALUES (1,'Александр','Мои кожа стала гладкой и шелковистой после посещения центра, а хорошее настроени сохранилось до сих пор!','2013-08-02 11:06:22','mail@example.com'),(2,'Лена Головач','Рекламная акция оправдывает департамент маркетинга и продаж, невзирая на действия конкурентов. Фирменный стиль поддерживает коллективный целевой сегмент рынка, осознав маркетинг как часть производства. Реклама программирует креативный формат события, оптимизируя бюджеты. Бизнес-модель недостаточно транслирует типичный социальный статус, повышая конкуренцию. ','2013-08-02 11:06:22','mail@example.com'),(3,'fdghfdgh','hdfgh','2013-08-13 15:20:04','dfgh@wer.ru');
/*!40000 ALTER TABLE `reviews_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_category`
--

DROP TABLE IF EXISTS `services_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_category`
--

LOCK TABLES `services_category` WRITE;
/*!40000 ALTER TABLE `services_category` DISABLE KEYS */;
INSERT INTO `services_category` VALUES (1,'Эстетическая косметология'),(2,'Лечебно-восстановительная медицина'),(3,'Эстетическая гинекология'),(4,'SPA-услуги');
/*!40000 ALTER TABLE `services_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_price`
--

DROP TABLE IF EXISTS `services_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(150) NOT NULL,
  `price` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_price_90e28c3e` (`service_id`),
  CONSTRAINT `service_id_refs_id_ebff4bf` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_price`
--

LOCK TABLES `services_price` WRITE;
/*!40000 ALTER TABLE `services_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `services_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_service`
--

DROP TABLE IF EXISTS `services_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `short_description` longtext NOT NULL,
  `description` longtext NOT NULL,
  `side_text` longtext NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_service_42dc49bc` (`category_id`),
  KEY `services_service_63f17a16` (`parent_id`),
  KEY `services_service_42b06ff6` (`lft`),
  KEY `services_service_6eabc1a6` (`rght`),
  KEY `services_service_102f80d8` (`tree_id`),
  KEY `services_service_2a8f42e8` (`level`),
  CONSTRAINT `category_id_refs_id_6727ca28979555f2` FOREIGN KEY (`category_id`) REFERENCES `services_subcategory` (`id`),
  CONSTRAINT `parent_id_refs_id_52064875` FOREIGN KEY (`parent_id`) REFERENCES `services_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_service`
--

LOCK TABLES `services_service` WRITE;
/*!40000 ALTER TABLE `services_service` DISABLE KEYS */;
INSERT INTO `services_service` VALUES (1,2,'Трихология','Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.','<p></p><p>Что волнует наших пациентов?</p><p><ul><li>60% мужчин и 58% женщин столкнулись с проблемой усиленного выпадения волос, ослабления волос и плохого их роста, проблемой сухих, чувствительных и поврежденных химическим воздействием волос.<br></li><li>65% мужчин и 45% женщин волнует проблема нарушения гидро-липидного баланса и нарушения работы сальных желез кожи головы.</li><li>60% мужчин и 40% женщин беспокоит проблема перхоти</li></ul></p><p>Как много нужно учесть, чтобы волосы, наконец, стали такими, какими их задумала природа – то есть красивыми, сильными, блестящими, эластичными. Чтобы достичь этого идеала была создана целая наука о волосах – трихология. Эта наука начала развиваться на Западе в начале ХХ века, а во второй его половине темпы её развития стали по-настоящему бурными: стали появляться трихологические центры, где лечили и оказывали услуги специально подготовленные профессионалы, стали возникать научно-исследовательские и учебные центры. Всем стало ясно, что красота волос – это производное их здоровья.</p><p>Современная индустрия красоты ориентируется не столько на внешний, декоративный эффект, сколько на улучшение здоровья отдельных систем и организма в целом.</p><p>Врач трихолог выявляет причины патологии кожи волосистой части головы.</p><p>Обычный прием врача-трихолога состоит из консультации с подбором лечебных средств для лечения в салоне и дополнительного ухода за волосам дома.</p><h2>Проблема выпадения волос</h2><p>Выпадение волос одна из самых серьезных проблем нашего времени. Стресс и дисфункции центральной и периферической нервной системы, дисфункция эндокринной системы, перенесенные тяжелые инфекционные заболевания, генетическая предрасположенность, аутоиммунная патология. Все это причины возникновения выпадения волос.</p><p>Решать данные проблемы в домашних условиях неправильно, тем более заниматься самолечением, покупая в аптеках общепринятые лечебные препараты. Выпадение волос нуждается не только в диагностическом исследовании, но и в правильном подборе индивидуальных лекарственных средств.</p><p>Лечение выпадения волос сложное, длительное, зависящее от клинической формы, стадии заболевания, возраста пациента. Часто лечение у врача-трихолога сочетается с лечением у смежных специалистов.</p><p>Какие есть теории и причины заболевания выпадения волос?</p><p><b>Аутоиммунная теория возникновения алопеции</b> – одна из наиболее актуальных на сегодняшний день. В нашем организме вырабатываются антитела к клеткам волосяного сосочка. В результате волосяные фолликулы преждевременно переходят из анагенной стадии в стадию телогена. В связи с длительно существующим инфильтратом иммунных клеток вокруг фолликула, который ухудшает его питание, следующий волос выходит ослабленным, дистрофически измененным. Таким образом, запускается процесс облысения.</p><p><b>Диффузное выпадение волос</b>. Клинически характеризуется повышенным выпадением волос, которое носит диффузный характер. Процесс может быть острым и хроническим.</p><p><b>Острое</b> начало заболевания вызывают тяжелые инфекции, психические и физические травмы, роды, операции под общим наркозом</p><p><b>Хроническое</b> выпадение волос возникает в результате эндокринных нарушений, длительного приема лекарственных препаратов, в том числе гормональных противозачаточных средств, с высоким содержанием прогестерона, нарушения питания и обмена веществ, возрастные изменения в организме, нехватка основных жирных кислот.</p><p><b>Андрогенное</b> облысение. Характеризуется поредением волос в лобной и теменной областях. Граница роста волос постепенно отступает со лба, а в центральной теменной области появляется поредение волос. Таким образом, формируется типичная картина андрогенного облысения. У женщин данный синдром может возникать через 2 месяца после родов, на фоне поликистоза яичников, в постменопаузном периоде</p><p><b>Себорейное</b> облысение. Связано с гормональным дисбалансом, с гиперсекрецией кожного сала и гиперкератозом, требует применения средств, уменьшающих салоотделение, уменьшающих размеры сальных желез и гиперкератоз.</p><h2>Лечение волос</h2><p>Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.<br></p><p>Решать данные проблемы в домашних условиях неправильно, тем более заниматься самолечением, покупая в аптеках общепринятые лечебные препараты. Выпадение волос нуждается не только в диагностическом исследовании, но и в правильном подборе индивидуальных лекарственных средств.</p><p>Лечение выпадения волос сложное, длительное, зависящее от клинической формы, стадии заболевания, возраста пациента.</p><p>В салоне « ASIA-SPA»наряду со множеством других методик лечения волос мы предлагаем Вам уникальное лечение волос и кожи головы новейшими инъекционными препаратами Mesopecia и Haircare.</p><p><ul><li><b>Mesopecia</b> – это мезотерапевтическое лечение,направленое на предупреждение и лечение андрогенного облысения ( алопеции ) у мужчн и женщин,улучшение трофики тканей волосистой части головы, усиление роста и качества волос,восстановление структуры повреждённых волос.<br></li><li>&nbsp;<b>Haircare</b>&nbsp;–&nbsp;этот препарат улучшает микроциркуляцию, повышает поступление кислорода, восстанавливает все виды обмена .Применяется при избыточном выпадении волос,преждевременном утрате пигмента ( седине), себорейном дерматите. Рекомендуемый курс-7-10 процедур, выполняются 1 раз в неделю, повторный курс через 6 месяцев.<br></li></ul></p><p>Аппаратная методика Дарсонваль — собирательное название группы медицинских приборов, основой действия которых являются импульсные переменные синусоидальные токи высокой частоты и напряжения (20 кВ), но малой силы (0,02 мА).Дарсонваль поможет вам в лечении волос (выпадении,перхоти,ломких, слабых волосах).</p><p>В результате комплексного воздействия улучшается кровообращение во всех областях скальпа, оказывается благотворное влияние на мозг, Вы освобождаетесь от всех видов стресса. Как результат - Вы получаете действительно здоровые ощущения, хорошее настроение и здоровые волосы.</p><p><br></p>\r\n','<p></p>Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.<p><br></p>\r\n',NULL,4,4,0,0),(2,2,'Дерматоонкология','Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>',NULL,4,4,0,0),(3,2,'Диагностика','Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>',2,0,1,0,1),(4,2,'Радиоволновое удаление','Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>',2,2,3,0,1);
/*!40000 ALTER TABLE `services_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_servicesign`
--

DROP TABLE IF EXISTS `services_servicesign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_servicesign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `initials` varchar(150) NOT NULL,
  `phone` varchar(150) NOT NULL,
  `desired_date` varchar(150) NOT NULL,
  `desired_time` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_servicesign_6f1d73c2` (`service_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_servicesign`
--

LOCK TABLES `services_servicesign` WRITE;
/*!40000 ALTER TABLE `services_servicesign` DISABLE KEYS */;
/*!40000 ALTER TABLE `services_servicesign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_subcategory`
--

DROP TABLE IF EXISTS `services_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_subcategory_42dc49bc` (`category_id`),
  CONSTRAINT `category_id_refs_id_e625ca552367b42` FOREIGN KEY (`category_id`) REFERENCES `services_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_subcategory`
--

LOCK TABLES `services_subcategory` WRITE;
/*!40000 ALTER TABLE `services_subcategory` DISABLE KEYS */;
INSERT INTO `services_subcategory` VALUES (2,1,'Терапевтическая косметология','<p>Это уход за кожей лица с помощью классических косметических средств и процедур без использования аппаратных методик. Разнообразие услуг по уходу за кожей в косметологических клиниках очень велико - это всевозможные пилинги, маски для лица, массаж, программы по уходу для разного возраста.</p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>\r\n'),(3,1,'Аппаратная косметология',''),(4,1,'Уходовые процедуры',''),(5,1,'Декоративная косметология',''),(6,2,'Дерматология','<p>Текстовое описание</p>\r\n'),(7,2,'Массаж','<p>Описание услуги</p>'),(8,2,'Мануальная терапия','<p>Текстовое описание<br></p>'),(9,2,'Гинекология','<p>Текстовое описание<br></p>'),(10,3,'Antiage гинекология','<p>Текстовое описание<br></p>'),(11,3,'Интимная контурная пластика','<p>Текстовое описание<br></p>'),(12,3,'Хирургическая коррекция малых половых губ','<p>Текстовое описание<br></p>'),(13,3,'Диагностика и лечение недержания мочи','<p>Текстовое описание<br></p>'),(14,4,'Обертывание','<p>Текстовое описание<br></p>'),(15,4,'Массажи','<p>Текстовое описание<br></p>'),(16,4,'SPA-программы','<p>Текстовое описание<br></p>');
/*!40000 ALTER TABLE `services_subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_clinicmenu`
--

DROP TABLE IF EXISTS `siteblocks_clinicmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_clinicmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `url` varchar(150) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_clinicmenu`
--

LOCK TABLES `siteblocks_clinicmenu` WRITE;
/*!40000 ALTER TABLE `siteblocks_clinicmenu` DISABLE KEYS */;
INSERT INTO `siteblocks_clinicmenu` VALUES (1,'Миссия и Политика','/clinic/about/',10,1),(2,'Общественная жизнь','/clinic/social/',10,1),(3,'Новости','/clinic/news/',10,1),(4,'Вакансии','/clinic/jobs/',10,1),(5,'Лицензии и сертификаты','/clinic/licence/',10,1);
/*!40000 ALTER TABLE `siteblocks_clinicmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_gallery`
--

DROP TABLE IF EXISTS `siteblocks_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `gallery_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_gallery`
--

LOCK TABLES `siteblocks_gallery` WRITE;
/*!40000 ALTER TABLE `siteblocks_gallery` DISABLE KEYS */;
INSERT INTO `siteblocks_gallery` VALUES (1,'Галлерея страницы \"О клинике\"',1,'about');
/*!40000 ALTER TABLE `siteblocks_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_galleryimage`
--

DROP TABLE IF EXISTS `siteblocks_galleryimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_galleryimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `show_on_index` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `siteblocks_galleryimage_34838cc3` (`gallery_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_galleryimage`
--

LOCK TABLES `siteblocks_galleryimage` WRITE;
/*!40000 ALTER TABLE `siteblocks_galleryimage` DISABLE KEYS */;
INSERT INTO `siteblocks_galleryimage` VALUES (1,1,'uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample.png',0),(2,1,'uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_1.png',0),(3,1,'uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_2.png',0),(4,1,'uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_3.png',0),(5,1,'uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_4.png',0),(6,1,'uploads/images/galleries/Gallereya_stranitsyi_\"O_klinike\"/preview-sample_5.png',0);
/*!40000 ALTER TABLE `siteblocks_galleryimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_settings`
--

DROP TABLE IF EXISTS `siteblocks_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_settings`
--

LOCK TABLES `siteblocks_settings` WRITE;
/*!40000 ALTER TABLE `siteblocks_settings` DISABLE KEYS */;
INSERT INTO `siteblocks_settings` VALUES (1,'Текст страницы \"О клинике\"','about_text','<p>Asia Spa - это оазис красоты и здоровья, в который можно войти, захлопнуть за собой дверь, оставив снаружи все свои проблемы. Cледуя философским традициям Востока и современности Запада, в самом центре Читы 20 декабря 2006 года открылся салон Asia Spa. Эксклюзивность салона в новизне и качестве услуг. Современный интерьер дополняется целостной концепцией терапии по уходу за лицом и телом.</p>\r\n\r\n<p>Своим клиентам Asia Spa предлагает широчайший ассортимент косметологического оборудования, который позволяет разрабатывать индивидуальные программы для каждого клиента. Это могут быть как отдельные курсы процедур, так и комплексные эстетические программы для лица и тела, решающие проблемы, возникающие в любом возрасте.</p>\r\n<br>','redactor'),(2,'Код карты','map','<script type=\"text/javascript\" charset=\"utf-8\" src=\"//api-maps.yandex.ru/services/constructor/1.0/js/?sid=MpnWs0Sqsp7DAqy7ZZzEg8PE07TqcFxI&width=560&height=394\"></script>','textarea');
/*!40000 ALTER TABLE `siteblocks_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_sitemenu`
--

DROP TABLE IF EXISTS `siteblocks_sitemenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_sitemenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `url` varchar(150) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `siteblocks_sitemenu_63f17a16` (`parent_id`),
  KEY `siteblocks_sitemenu_42b06ff6` (`lft`),
  KEY `siteblocks_sitemenu_91543e5a` (`rght`),
  KEY `siteblocks_sitemenu_efd07f28` (`tree_id`),
  KEY `siteblocks_sitemenu_2a8f42e8` (`level`),
  CONSTRAINT `parent_id_refs_id_195cda43247648db` FOREIGN KEY (`parent_id`) REFERENCES `siteblocks_sitemenu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_sitemenu`
--

LOCK TABLES `siteblocks_sitemenu` WRITE;
/*!40000 ALTER TABLE `siteblocks_sitemenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `siteblocks_sitemenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_slideritem`
--

DROP TABLE IF EXISTS `siteblocks_slideritem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_slideritem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `slot` varchar(150) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `title` varchar(150) NOT NULL,
  `link` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_slideritem`
--

LOCK TABLES `siteblocks_slideritem` WRITE;
/*!40000 ALTER TABLE `siteblocks_slideritem` DISABLE KEYS */;
INSERT INTO `siteblocks_slideritem` VALUES (1,'uploads/images/slider/slider-big-sample.png','big',1,'',''),(2,'uploads/images/slider/slider-small-sample.png','small_top',1,'',''),(3,'uploads/images/slider/slider-small-sample_1.png','small_bottom',1,'',''),(4,'uploads/images/slider/slider-medium-sample.png','medium_top',1,'',''),(5,'uploads/images/slider/slider-medium-sample_1.png','medium_bottom',1,'','');
/*!40000 ALTER TABLE `siteblocks_slideritem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'menu','0001_initial','2013-08-02 10:49:10'),(2,'dashboard','0001_initial','2013-08-02 10:49:10'),(3,'dashboard','0002_auto__add_field_dashboardpreferences_dashboard_id','2013-08-02 10:49:11'),(4,'dashboard','0003_auto__add_unique_dashboardpreferences_dashboard_id_user','2013-08-02 10:49:11'),(5,'siteblocks','0001_initial','2013-08-02 10:49:12'),(6,'pages','0001_initial','2013-08-02 10:49:14'),(7,'newsboard','0001_initial','2013-08-02 10:49:15'),(8,'specs','0001_initial','2013-08-02 10:49:16'),(9,'specs','0002_auto__chg_field_spec_title','2013-08-02 10:49:16'),(10,'reviews','0001_initial','2013-08-02 10:49:16'),(11,'reviews','0002_auto__del_field_review_phone','2013-08-02 10:49:16'),(12,'reviews','0003_auto__add_field_review_email','2013-08-02 10:49:17'),(13,'services','0001_initial','2013-08-02 14:29:59'),(14,'services','0002_auto__del_serviceprice__add_price','2013-08-02 14:55:25'),(15,'services','0003_auto__add_field_category_slug','2013-08-02 15:27:10'),(16,'services','0004_auto__del_field_category_slug__del_field_subservice_slug__del_field_se','2013-08-02 16:35:35'),(17,'services','0005_auto__add_field_subcategory_description','2013-08-02 18:17:18'),(18,'services','0006_auto__del_field_subservice_short_description','2013-08-02 18:48:21'),(19,'siteblocks','0002_auto__add_clinicmenu','2013-08-13 12:32:15'),(20,'siteblocks','0003_auto__add_gallery__add_galleryimage','2013-08-13 12:32:15'),(21,'services','0007_auto__del_subservice__add_sevicesign__add_field_service_parent__add_fi','2013-08-13 12:32:16'),(22,'services','0008_auto__del_sevicesign__add_servicesign','2013-08-13 12:32:16'),(23,'services','0009_auto__chg_field_servicesign_desired_time__chg_field_servicesign_desire','2013-08-13 12:32:17'),(24,'articles','0001_initial','2013-08-13 13:12:29'),(25,'siteblocks','0004_auto__add_slideritem','2013-08-13 14:05:00'),(26,'articles','0002_auto__del_field_social_short_text','2013-08-14 12:03:06');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specs_question`
--

DROP TABLE IF EXISTS `specs_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specs_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_id` int(11) NOT NULL,
  `question` longtext NOT NULL,
  `email` varchar(75) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `specs_question_1d8e9887` (`spec_id`),
  CONSTRAINT `spec_id_refs_id_5eb07ecf28082bee` FOREIGN KEY (`spec_id`) REFERENCES `specs_spec` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specs_question`
--

LOCK TABLES `specs_question` WRITE;
/*!40000 ALTER TABLE `specs_question` DISABLE KEYS */;
INSERT INTO `specs_question` VALUES (1,1,'qwer','qwer@example.com');
/*!40000 ALTER TABLE `specs_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specs_spec`
--

DROP TABLE IF EXISTS `specs_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specs_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initials` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  `short_desc` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specs_spec`
--

LOCK TABLES `specs_spec` WRITE;
/*!40000 ALTER TABLE `specs_spec` DISABLE KEYS */;
INSERT INTO `specs_spec` VALUES (1,'Дугарова Дарима Гатыповна','Главный врач, дерматокосметолог','images/spec/1.png','Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.','<p>Рекламная площадка ускоряет межличностный рекламный клаттер, не считаясь с затратами. Узнавание бренда, отбрасывая подробности, амбивалентно. Российская специфика, безусловно, искажает направленный маркетинг, расширяя долю рынка. Рыночная информация,&nbsp;как&nbsp;следует&nbsp;из&nbsp;вышесказанного, экономит экспериментальный медийный канал, осознав маркетинг как часть производства. До недавнего времени считалось, что жизненный цикл продукции повсеместно экономит отраслевой стандарт, повышая конкуренцию.</p><p>Косвенная реклама требовальна к креативу. Представляется&nbsp;логичным,&nbsp;что рекламное сообщество изменяет связанный продукт, не считаясь с затратами. Пресс-клиппинг,&nbsp;в&nbsp;рамках&nbsp;сегодняшних&nbsp;воззрений, синхронизирует из ряда вон выходящий conversion rate, осознав маркетинг как часть производства. Воздействие на потребителя искажает эмпирический показ баннера, оптимизируя бюджеты. Социальная ответственность спонтанно тормозит медиабизнес, повышая конкуренцию.</p>\r\n'),(2,'Фамилия Имя Отчество','Должность','images/spec/2.png','Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.','<p>Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.&nbsp;Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.&nbsp;Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.<br></p>'),(3,'Фамилия Имя Отчество','Должность','images/spec/3.png','Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.','<p>Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.&nbsp;Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.&nbsp;Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.<br></p>'),(4,'Фамилия Имя Отчество','Должность','images/spec/4.png','Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.','<p>Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.&nbsp;Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.&nbsp;Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.<br></p>');
/*!40000 ALTER TABLE `specs_spec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
INSERT INTO `thumbnail_kvstore` VALUES ('sorl-thumbnail||image||00a272b1b048b220db0d0c93fd18d9a2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/spec/2.png\", \"size\": [149, 172]}'),('sorl-thumbnail||image||0134776c77f1bd9a4d8e136c03b5c08f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a8/22/a8227b6e8a5b8fea9da71fe45d8609d8.png\", \"size\": [300, 144]}'),('sorl-thumbnail||image||04a1266b51d32746da5ef3ba6c6dd971','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/galleries/Gallereya_stranitsyi_\\\"O_klinike\\\"/preview-sample_4.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||0946bc2cf798f80d754a59494c29313b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/58/47/584718a7b77146c56f891a9b646c0e64.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||0a8f66430f243d3202cf46f0d1c09dc8','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/galleries/Gallereya_stranitsyi_\\\"O_klinike\\\"/preview-sample_3.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||0eeb0ff292c762db8eb4bd19a3da8510','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/34/1e/341ee05cf3fcec8b749a1d00ee0e13dc.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||1063c0376a16cca1333b99ce4a999c67','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/f9/a8/f9a863ad678391e8617cda70b5bca602.png\", \"size\": [149, 174]}'),('sorl-thumbnail||image||129440d663bac05043535ae86309aca6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/38/90/38903a989d56bd9d0352ca197a0bdfea.jpg\", \"size\": [149, 174]}'),('sorl-thumbnail||image||1d19a37a47fd28128181c4a93813cc46','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ce/e1/cee14710ac31a9d3f55eb8604e129364.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||28fefc92a35f82fa53f336e29e90892b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/spec/1.png\", \"size\": [149, 174]}'),('sorl-thumbnail||image||34c442b4c1ab681582f6035e326eba2c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/spec/person-example-2_1.png\", \"size\": [149, 174]}'),('sorl-thumbnail||image||41f5d06ec3318e8f14ea37d3324fff5c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/42/58/425873f86f70bcc07e0f5669654799d7.png\", \"size\": [160, 144]}'),('sorl-thumbnail||image||51fbea5421faa17375db09d489cd53de','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/slider/slider-big-sample.png\", \"size\": [260, 305]}'),('sorl-thumbnail||image||5dabbc135480ad6082c520ed90272b07','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/galleries/Gallereya_stranitsyi_\\\"O_klinike\\\"/preview-sample_2.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||6320266c84a74880cc0e4b32bab155f4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/9b/c4/9bc4a313bc5e5e0a655b10264733673b.png\", \"size\": [160, 144]}'),('sorl-thumbnail||image||6e13c3c30dcafa1621e41eea96b65c6d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/slider/slider-medium-sample_1.png\", \"size\": [300, 144]}'),('sorl-thumbnail||image||704e339a7b09f049af43d33602b26c69','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/social/preview-sample2_1.png\", \"size\": [160, 108]}'),('sorl-thumbnail||image||7ea68b31d8ebf05e138676c5d71b1f75','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/galleries/Gallereya_stranitsyi_\\\"O_klinike\\\"/preview-sample_1.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||82c16cc8d423d0f97226f0b52790a118','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/6a/e9/6ae9957bc8320621e1d89301a3942492.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||a34ba31953a381f3abbb10f024b8cfc2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/galleries/Gallereya_stranitsyi_\\\"O_klinike\\\"/preview-sample.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||a60e770ba843401f8454acc1570ac752','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/6a/72/6a723bd48c6929cf1b4a1fe055d4fcb4.png\", \"size\": [260, 305]}'),('sorl-thumbnail||image||b18fdc019dd4f05a5665c857fccd4468','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/slider/slider-small-sample_1.png\", \"size\": [160, 144]}'),('sorl-thumbnail||image||b89f817816e080ddc369d9107a6a7b48','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/6f/55/6f55c0f2f04a0973f793dabe7b7a93d6.png\", \"size\": [149, 161]}'),('sorl-thumbnail||image||b9391373ec8973a08dc8ff1a39785815','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/63/de/63def71a3aeb8f715c886ae63e391c75.png\", \"size\": [300, 144]}'),('sorl-thumbnail||image||c377489bd211e414c2b986677fabab6d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/6e/ee/6eee570e1fb64635b2fbe83d92048fb3.png\", \"size\": [149, 161]}'),('sorl-thumbnail||image||c3876856f35b26bae848b791c7bdbda2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/slider/slider-medium-sample.png\", \"size\": [300, 144]}'),('sorl-thumbnail||image||c64bb69ef722d12c5b61360e229850ad','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/d3/0e/d30e3a6717bb5d4c20297446aa60a1b2.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||cf29d85de3f14e9d7b653ac26d5f6751','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/45/fe/45fed686dbccddd9318cd0e4b8cb047c.png\", \"size\": [160, 108]}'),('sorl-thumbnail||image||d41f52a78ef36ea4c8c0bda326b3981a','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/52/81/52819e85c4dcd2fde813c45ee45f0315.png\", \"size\": [160, 108]}'),('sorl-thumbnail||image||d737687b12977fc03c12ec417221913e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/spec/4.png\", \"size\": [149, 161]}'),('sorl-thumbnail||image||d85df70b46e56d0e0e4e69f1001832db','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/de/03/de03c6f047a78fa6dcb1c043ec48dfc5.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||db36e4d63380940192d183f503a0bfff','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/spec/3.png\", \"size\": [149, 161]}'),('sorl-thumbnail||image||dd2373b4e9fa5c3eb44ce77929319af7','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/slider/slider-small-sample.png\", \"size\": [160, 144]}'),('sorl-thumbnail||image||dd36bdcf117ef0430f1f24b250655b0b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/images/galleries/Gallereya_stranitsyi_\\\"O_klinike\\\"/preview-sample_5.png\", \"size\": [160, 92]}'),('sorl-thumbnail||image||dffd32b817ec4e9f90facb85c5025cb6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/dc/b4/dcb4a2c405e910f6e53966886bf9bcf1.png\", \"size\": [149, 174]}'),('sorl-thumbnail||image||eb2a993dd94a72860128fd57b33e79ec','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/social/preview-sample2.png\", \"size\": [160, 108]}'),('sorl-thumbnail||image||f8d9ce46b8dbe4f8341709991da2aa9a','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/cb/90/cb90ae2a48accff02a562effe7570bb7.png\", \"size\": [149, 172]}'),('sorl-thumbnail||thumbnails||00a272b1b048b220db0d0c93fd18d9a2','[\"f8d9ce46b8dbe4f8341709991da2aa9a\"]'),('sorl-thumbnail||thumbnails||04a1266b51d32746da5ef3ba6c6dd971','[\"0eeb0ff292c762db8eb4bd19a3da8510\"]'),('sorl-thumbnail||thumbnails||0a8f66430f243d3202cf46f0d1c09dc8','[\"82c16cc8d423d0f97226f0b52790a118\"]'),('sorl-thumbnail||thumbnails||28fefc92a35f82fa53f336e29e90892b','[\"1063c0376a16cca1333b99ce4a999c67\"]'),('sorl-thumbnail||thumbnails||34c442b4c1ab681582f6035e326eba2c','[\"dffd32b817ec4e9f90facb85c5025cb6\", \"129440d663bac05043535ae86309aca6\"]'),('sorl-thumbnail||thumbnails||51fbea5421faa17375db09d489cd53de','[\"a60e770ba843401f8454acc1570ac752\"]'),('sorl-thumbnail||thumbnails||5dabbc135480ad6082c520ed90272b07','[\"d85df70b46e56d0e0e4e69f1001832db\"]'),('sorl-thumbnail||thumbnails||6e13c3c30dcafa1621e41eea96b65c6d','[\"b9391373ec8973a08dc8ff1a39785815\"]'),('sorl-thumbnail||thumbnails||704e339a7b09f049af43d33602b26c69','[\"cf29d85de3f14e9d7b653ac26d5f6751\"]'),('sorl-thumbnail||thumbnails||7ea68b31d8ebf05e138676c5d71b1f75','[\"1d19a37a47fd28128181c4a93813cc46\"]'),('sorl-thumbnail||thumbnails||a34ba31953a381f3abbb10f024b8cfc2','[\"c64bb69ef722d12c5b61360e229850ad\"]'),('sorl-thumbnail||thumbnails||b18fdc019dd4f05a5665c857fccd4468','[\"6320266c84a74880cc0e4b32bab155f4\"]'),('sorl-thumbnail||thumbnails||c3876856f35b26bae848b791c7bdbda2','[\"0134776c77f1bd9a4d8e136c03b5c08f\"]'),('sorl-thumbnail||thumbnails||d737687b12977fc03c12ec417221913e','[\"c377489bd211e414c2b986677fabab6d\"]'),('sorl-thumbnail||thumbnails||db36e4d63380940192d183f503a0bfff','[\"b89f817816e080ddc369d9107a6a7b48\"]'),('sorl-thumbnail||thumbnails||dd2373b4e9fa5c3eb44ce77929319af7','[\"41f5d06ec3318e8f14ea37d3324fff5c\"]'),('sorl-thumbnail||thumbnails||dd36bdcf117ef0430f1f24b250655b0b','[\"0946bc2cf798f80d754a59494c29313b\"]'),('sorl-thumbnail||thumbnails||eb2a993dd94a72860128fd57b33e79ec','[\"d41f52a78ef36ea4c8c0bda326b3981a\"]');
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-14 13:22:27
