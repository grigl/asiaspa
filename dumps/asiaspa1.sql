-- MySQL dump 10.13  Distrib 5.6.10, for osx10.6 (x86_64)
--
-- Host: localhost    Database: asiaspa
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tools_dashboard_prefer_dashboard_id_374bce90a8a4eefc_uniq` (`dashboard_id`,`user_id`),
  KEY `admin_tools_dashboard_preferences_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_2faedda1f8487376` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{}','dashboard'),(2,1,'{}','specs-dashboard'),(3,1,'{}','services-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_6af2836063b2844f` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add log entry',7,'add_logentry'),(20,'Can change log entry',7,'change_logentry'),(21,'Can delete log entry',7,'delete_logentry'),(22,'Can add kv store',8,'add_kvstore'),(23,'Can change kv store',8,'change_kvstore'),(24,'Can delete kv store',8,'delete_kvstore'),(25,'Can add migration history',9,'add_migrationhistory'),(26,'Can change migration history',9,'change_migrationhistory'),(27,'Can delete migration history',9,'delete_migrationhistory'),(28,'Can add bookmark',10,'add_bookmark'),(29,'Can change bookmark',10,'change_bookmark'),(30,'Can delete bookmark',10,'delete_bookmark'),(31,'Can add dashboard preferences',11,'add_dashboardpreferences'),(32,'Can change dashboard preferences',11,'change_dashboardpreferences'),(33,'Can delete dashboard preferences',11,'delete_dashboardpreferences'),(34,'Can add menu_item',12,'add_sitemenu'),(35,'Can change menu_item',12,'change_sitemenu'),(36,'Can delete menu_item',12,'delete_sitemenu'),(37,'Can add site_setting',13,'add_settings'),(38,'Can change site_setting',13,'change_settings'),(39,'Can delete site_setting',13,'delete_settings'),(40,'Can add page_item',14,'add_page'),(41,'Can change page_item',14,'change_page'),(42,'Can delete page_item',14,'delete_page'),(43,'Can add файл',15,'add_pagedoc'),(44,'Can change файл',15,'change_pagedoc'),(45,'Can delete файл',15,'delete_pagedoc'),(46,'Can add картинка',16,'add_pagepic'),(47,'Can change картинка',16,'change_pagepic'),(48,'Can delete картинка',16,'delete_pagepic'),(49,'Can add meta',17,'add_metadata'),(50,'Can change meta',17,'change_metadata'),(51,'Can delete meta',17,'delete_metadata'),(52,'Can add news_category',18,'add_newscategory'),(53,'Can change news_category',18,'change_newscategory'),(54,'Can delete news_category',18,'delete_newscategory'),(55,'Can add news_item',19,'add_news'),(56,'Can change news_item',19,'change_news'),(57,'Can delete news_item',19,'delete_news'),(58,'Can add специалист',20,'add_spec'),(59,'Can change специалист',20,'change_spec'),(60,'Can delete специалист',20,'delete_spec'),(61,'Can add вопрос',21,'add_question'),(62,'Can change вопрос',21,'change_question'),(63,'Can delete вопрос',21,'delete_question'),(64,'Can add отзыв',22,'add_review'),(65,'Can change отзыв',22,'change_review'),(66,'Can delete отзыв',22,'delete_review'),(67,'Can add раздел',23,'add_category'),(68,'Can change раздел',23,'change_category'),(69,'Can delete раздел',23,'delete_category'),(70,'Can add подраздел',24,'add_subcategory'),(71,'Can change подраздел',24,'change_subcategory'),(72,'Can delete подраздел',24,'delete_subcategory'),(73,'Can add услуга',25,'add_service'),(74,'Can change услуга',25,'change_service'),(75,'Can delete услуга',25,'delete_service'),(76,'Can add подуслуга',26,'add_subservice'),(77,'Can change подуслуга',26,'change_subservice'),(78,'Can delete подуслуга',26,'delete_subservice'),(82,'Can add цена услуги',28,'add_price'),(83,'Can change цена услуги',28,'change_price'),(84,'Can delete цена услуги',28,'delete_price');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'grigl','','','grigl@mail.ru','pbkdf2_sha256$10000$Q2ZD49Fv3HMK$Gx3/dkIUzBO7lKbdQJpEtaXKlccT43Wjo0vkzPCLBhQ=',1,1,1,'2013-08-02 11:11:19','2013-08-02 10:49:00');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`),
  CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`),
  CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2013-08-02 11:27:29',1,20,'1','Дугарова Дарима Гатыповна',1,''),(2,'2013-08-02 11:50:13',1,20,'1','Дугарова Дарима Гатыповна',2,'Изменен description.'),(3,'2013-08-02 11:50:26',1,20,'1','Дугарова Дарима Гатыповна',2,'Изменен description.'),(4,'2013-08-02 11:50:44',1,20,'1','Дугарова Дарима Гатыповна',2,'Изменен description.'),(5,'2013-08-02 15:03:32',1,23,'1','Эстетическая косметология',1,''),(6,'2013-08-02 15:09:36',1,24,'1','Терапевтическая косметология',1,''),(7,'2013-08-02 15:10:10',1,24,'1','Терапевтическая косметология',3,''),(8,'2013-08-02 15:11:18',1,24,'2','Терапевтическая косметология',1,''),(9,'2013-08-02 15:11:46',1,24,'2','Терапевтическая косметология',2,'Ни одно поле не изменено.'),(10,'2013-08-02 15:11:58',1,24,'2','Терапевтическая косметология',2,'Ни одно поле не изменено.'),(11,'2013-08-02 15:12:35',1,24,'2','Терапевтическая косметология',2,'Изменен slug.'),(12,'2013-08-02 15:13:28',1,24,'3','Аппаратная косметология',1,''),(13,'2013-08-02 15:20:56',1,24,'4','Уходовые процедуры',1,''),(14,'2013-08-02 15:24:44',1,24,'5','Декоративная косметология',1,''),(15,'2013-08-02 15:27:21',1,23,'1','Эстетическая косметология',2,'Ни одно поле не изменено.'),(16,'2013-08-02 18:19:26',1,24,'2','Терапевтическая косметология',2,'Изменен description.'),(17,'2013-08-02 18:32:07',1,25,'1','Трихология',1,''),(18,'2013-08-02 18:33:05',1,25,'2','Дерматоонкология',1,''),(19,'2013-08-02 18:48:29',1,26,'1','Диагностика',1,''),(20,'2013-08-02 18:49:07',1,26,'2','Радиоволновое удаление',1,''),(21,'2013-08-02 18:49:38',1,26,'3','Электрокоагуляция',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'log entry','admin','logentry'),(8,'kv store','thumbnail','kvstore'),(9,'migration history','south','migrationhistory'),(10,'bookmark','menu','bookmark'),(11,'dashboard preferences','dashboard','dashboardpreferences'),(12,'menu_item','siteblocks','sitemenu'),(13,'site_setting','siteblocks','settings'),(14,'page_item','pages','page'),(15,'файл','pages','pagedoc'),(16,'картинка','pages','pagepic'),(17,'meta','pages','metadata'),(18,'news_category','newsboard','newscategory'),(19,'news_item','newsboard','news'),(20,'специалист','specs','spec'),(21,'вопрос','specs','question'),(22,'отзыв','reviews','review'),(23,'раздел','services','category'),(24,'подраздел','services','subcategory'),(25,'услуга','services','service'),(26,'подуслуга','services','subservice'),(28,'цена услуги','services','price');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('ca861e83752061053afe2ed25d24cf3a','N2MzYzdlM2MzOTdlNDY0ODc4NTM0ODk0MDI1M2NlM2EyYzQ4ODFmMjqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-08-16 11:11:19');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_news`
--

DROP TABLE IF EXISTS `newsboard_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  `short_text` longtext NOT NULL,
  `text` longtext NOT NULL,
  `on_main_page` tinyint(1) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `newsboard_news_42dc49bc` (`category_id`),
  CONSTRAINT `category_id_refs_id_4d334043d0f945e1` FOREIGN KEY (`category_id`) REFERENCES `newsboard_newscategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_news`
--

LOCK TABLES `newsboard_news` WRITE;
/*!40000 ALTER TABLE `newsboard_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsboard_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsboard_newscategory`
--

DROP TABLE IF EXISTS `newsboard_newscategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboard_newscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsboard_newscategory`
--

LOCK TABLES `newsboard_newscategory` WRITE;
/*!40000 ALTER TABLE `newsboard_newscategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsboard_newscategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_metadata`
--

DROP TABLE IF EXISTS `pages_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_metadata`
--

LOCK TABLES `pages_metadata` WRITE;
/*!40000 ALTER TABLE `pages_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_page`
--

DROP TABLE IF EXISTS `pages_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  `title` varchar(120) NOT NULL,
  `url` varchar(200) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `is_at_menu` tinyint(1) NOT NULL,
  `is_at_footer_menu` tinyint(1) NOT NULL,
  `template` varchar(100) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `pages_page_63f17a16` (`parent_id`),
  KEY `pages_page_42b06ff6` (`lft`),
  KEY `pages_page_91543e5a` (`rght`),
  KEY `pages_page_efd07f28` (`tree_id`),
  KEY `pages_page_2a8f42e8` (`level`),
  CONSTRAINT `parent_id_refs_id_3eab4d20353c0eed` FOREIGN KEY (`parent_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_page`
--

LOCK TABLES `pages_page` WRITE;
/*!40000 ALTER TABLE `pages_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagedoc`
--

DROP TABLE IF EXISTS `pages_pagedoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagedoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagedoc_32d04bc7` (`page_id`),
  CONSTRAINT `page_id_refs_id_c5e8c32df41e052` FOREIGN KEY (`page_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagedoc`
--

LOCK TABLES `pages_pagedoc` WRITE;
/*!40000 ALTER TABLE `pages_pagedoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagedoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagepic`
--

DROP TABLE IF EXISTS `pages_pagepic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagepic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagepic_32d04bc7` (`page_id`),
  CONSTRAINT `page_id_refs_id_4711677842aca3ec` FOREIGN KEY (`page_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagepic`
--

LOCK TABLES `pages_pagepic` WRITE;
/*!40000 ALTER TABLE `pages_pagepic` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagepic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews_review`
--

DROP TABLE IF EXISTS `reviews_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initials` varchar(150) NOT NULL,
  `review` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `email` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews_review`
--

LOCK TABLES `reviews_review` WRITE;
/*!40000 ALTER TABLE `reviews_review` DISABLE KEYS */;
INSERT INTO `reviews_review` VALUES (1,'Александр','Мои кожа стала гладкой и шелковистой после посещения центра, а хорошее настроени сохранилось до сих пор!','2013-08-02 11:06:22','mail@example.com'),(2,'Лена Головач','Рекламная акция оправдывает департамент маркетинга и продаж, невзирая на действия конкурентов. Фирменный стиль поддерживает коллективный целевой сегмент рынка, осознав маркетинг как часть производства. Реклама программирует креативный формат события, оптимизируя бюджеты. Бизнес-модель недостаточно транслирует типичный социальный статус, повышая конкуренцию. ','2013-08-02 11:06:22','mail@example.com');
/*!40000 ALTER TABLE `reviews_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_category`
--

DROP TABLE IF EXISTS `services_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_category`
--

LOCK TABLES `services_category` WRITE;
/*!40000 ALTER TABLE `services_category` DISABLE KEYS */;
INSERT INTO `services_category` VALUES (1,'Эстетическая косметология');
/*!40000 ALTER TABLE `services_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_price`
--

DROP TABLE IF EXISTS `services_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `subservice_id` int(11) DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `price` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_price_90e28c3e` (`service_id`),
  KEY `services_price_5645238b` (`subservice_id`),
  CONSTRAINT `service_id_refs_id_21a637850ebff4bf` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`),
  CONSTRAINT `subservice_id_refs_id_51c61b764a7d9c8a` FOREIGN KEY (`subservice_id`) REFERENCES `services_subservice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_price`
--

LOCK TABLES `services_price` WRITE;
/*!40000 ALTER TABLE `services_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `services_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_service`
--

DROP TABLE IF EXISTS `services_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `short_description` longtext NOT NULL,
  `description` longtext NOT NULL,
  `side_text` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_service_42dc49bc` (`category_id`),
  CONSTRAINT `category_id_refs_id_6727ca28979555f2` FOREIGN KEY (`category_id`) REFERENCES `services_subcategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_service`
--

LOCK TABLES `services_service` WRITE;
/*!40000 ALTER TABLE `services_service` DISABLE KEYS */;
INSERT INTO `services_service` VALUES (1,2,'Трихология','Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.','<p>Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.<br></p><p>Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.<br></p>','<p>Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.<br></p><p>Красивыми в наше время считаются только здоровые ухоженные волосы, вне зависимости от их длины. А на пути к здоровым сильным волосам сейчас появилось множество препятствий: плохая экология, жёсткая вода, недостаток витаминов, несбалансированное питание, стресс, внутренние заболевания и неправильный уход за волосами.<br></p>'),(2,2,'Дерматоонкология','Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>','<p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p><p>Это наука, изучающая новообразования и опухолевидные пороки развития кожи. Клинический опыт врачей, занимающихся проблемой новообразований кожи, заметно обогатился в последние годы, разработаны новые классификации, изучены механизмы возникновения, разработаны рациональные подходы к лечению новообразований.<br></p>');
/*!40000 ALTER TABLE `services_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_subcategory`
--

DROP TABLE IF EXISTS `services_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_subcategory_42dc49bc` (`category_id`),
  CONSTRAINT `category_id_refs_id_e625ca552367b42` FOREIGN KEY (`category_id`) REFERENCES `services_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_subcategory`
--

LOCK TABLES `services_subcategory` WRITE;
/*!40000 ALTER TABLE `services_subcategory` DISABLE KEYS */;
INSERT INTO `services_subcategory` VALUES (2,1,'Терапевтическая косметология','<p>Это уход за кожей лица с помощью классических косметических средств и процедур без использования аппаратных методик. Разнообразие услуг по уходу за кожей в косметологических клиниках очень велико - это всевозможные пилинги, маски для лица, массаж, программы по уходу для разного возраста.</p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>\r\n'),(3,1,'Аппаратная косметология',''),(4,1,'Уходовые процедуры',''),(5,1,'Декоративная косметология','');
/*!40000 ALTER TABLE `services_subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_subservice`
--

DROP TABLE IF EXISTS `services_subservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_subservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `side_text` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `services_subservice_90e28c3e` (`service_id`),
  CONSTRAINT `service_id_refs_id_1b06f2881bcf3e30` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_subservice`
--

LOCK TABLES `services_subservice` WRITE;
/*!40000 ALTER TABLE `services_subservice` DISABLE KEYS */;
INSERT INTO `services_subservice` VALUES (1,2,'Диагностика','<p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>','<p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>'),(2,2,'Радиоволновое удаление','<p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>','<p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>'),(3,2,'Электрокоагуляция','<p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>','<p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p><p>Врачи-косметологи клиники «Asia-Spa» индивидуально разрабатывают курс процедур с учетом всех пожеланий клиентов. Какая бы услуга по косметологии лица вам не понадобилась - регулярная ухаживающая маска, соответствующая вашему типу кожи, массаж или чистка лица – вы непременно останетесь довольны результатом.<br></p>');
/*!40000 ALTER TABLE `services_subservice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_settings`
--

DROP TABLE IF EXISTS `siteblocks_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_settings`
--

LOCK TABLES `siteblocks_settings` WRITE;
/*!40000 ALTER TABLE `siteblocks_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `siteblocks_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_sitemenu`
--

DROP TABLE IF EXISTS `siteblocks_sitemenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_sitemenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `url` varchar(150) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `siteblocks_sitemenu_63f17a16` (`parent_id`),
  KEY `siteblocks_sitemenu_42b06ff6` (`lft`),
  KEY `siteblocks_sitemenu_91543e5a` (`rght`),
  KEY `siteblocks_sitemenu_efd07f28` (`tree_id`),
  KEY `siteblocks_sitemenu_2a8f42e8` (`level`),
  CONSTRAINT `parent_id_refs_id_195cda43247648db` FOREIGN KEY (`parent_id`) REFERENCES `siteblocks_sitemenu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_sitemenu`
--

LOCK TABLES `siteblocks_sitemenu` WRITE;
/*!40000 ALTER TABLE `siteblocks_sitemenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `siteblocks_sitemenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'menu','0001_initial','2013-08-02 10:49:10'),(2,'dashboard','0001_initial','2013-08-02 10:49:10'),(3,'dashboard','0002_auto__add_field_dashboardpreferences_dashboard_id','2013-08-02 10:49:11'),(4,'dashboard','0003_auto__add_unique_dashboardpreferences_dashboard_id_user','2013-08-02 10:49:11'),(5,'siteblocks','0001_initial','2013-08-02 10:49:12'),(6,'pages','0001_initial','2013-08-02 10:49:14'),(7,'newsboard','0001_initial','2013-08-02 10:49:15'),(8,'specs','0001_initial','2013-08-02 10:49:16'),(9,'specs','0002_auto__chg_field_spec_title','2013-08-02 10:49:16'),(10,'reviews','0001_initial','2013-08-02 10:49:16'),(11,'reviews','0002_auto__del_field_review_phone','2013-08-02 10:49:16'),(12,'reviews','0003_auto__add_field_review_email','2013-08-02 10:49:17'),(13,'services','0001_initial','2013-08-02 14:29:59'),(14,'services','0002_auto__del_serviceprice__add_price','2013-08-02 14:55:25'),(15,'services','0003_auto__add_field_category_slug','2013-08-02 15:27:10'),(16,'services','0004_auto__del_field_category_slug__del_field_subservice_slug__del_field_se','2013-08-02 16:35:35'),(17,'services','0005_auto__add_field_subcategory_description','2013-08-02 18:17:18'),(18,'services','0006_auto__del_field_subservice_short_description','2013-08-02 18:48:21');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specs_question`
--

DROP TABLE IF EXISTS `specs_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specs_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_id` int(11) NOT NULL,
  `question` longtext NOT NULL,
  `email` varchar(75) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `specs_question_1d8e9887` (`spec_id`),
  CONSTRAINT `spec_id_refs_id_5eb07ecf28082bee` FOREIGN KEY (`spec_id`) REFERENCES `specs_spec` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specs_question`
--

LOCK TABLES `specs_question` WRITE;
/*!40000 ALTER TABLE `specs_question` DISABLE KEYS */;
INSERT INTO `specs_question` VALUES (1,1,'qwer','qwer@example.com');
/*!40000 ALTER TABLE `specs_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specs_spec`
--

DROP TABLE IF EXISTS `specs_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specs_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initials` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  `short_desc` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specs_spec`
--

LOCK TABLES `specs_spec` WRITE;
/*!40000 ALTER TABLE `specs_spec` DISABLE KEYS */;
INSERT INTO `specs_spec` VALUES (1,'Дугарова Дарима Гатыповна','Главный врач, дерматокосметолог','images/spec/person-example-2_1.png','Небольшое описание врача (1-2 абзаца) с информацией где учились, курсы повышения квалификации, профессиональные достижения. Стаж работы.','<p>Рекламная площадка ускоряет межличностный рекламный клаттер, не считаясь с затратами. Узнавание бренда, отбрасывая подробности, амбивалентно. Российская специфика, безусловно, искажает направленный маркетинг, расширяя долю рынка. Рыночная информация,&nbsp;как&nbsp;следует&nbsp;из&nbsp;вышесказанного, экономит экспериментальный медийный канал, осознав маркетинг как часть производства. До недавнего времени считалось, что жизненный цикл продукции повсеместно экономит отраслевой стандарт, повышая конкуренцию.</p><p>Косвенная реклама требовальна к креативу. Представляется&nbsp;логичным,&nbsp;что рекламное сообщество изменяет связанный продукт, не считаясь с затратами. Пресс-клиппинг,&nbsp;в&nbsp;рамках&nbsp;сегодняшних&nbsp;воззрений, синхронизирует из ряда вон выходящий conversion rate, осознав маркетинг как часть производства. Воздействие на потребителя искажает эмпирический показ баннера, оптимизируя бюджеты. Социальная ответственность спонтанно тормозит медиабизнес, повышая конкуренцию.</p>\r\n');
/*!40000 ALTER TABLE `specs_spec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
INSERT INTO `thumbnail_kvstore` VALUES ('sorl-thumbnail||image||129440d663bac05043535ae86309aca6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/38/90/38903a989d56bd9d0352ca197a0bdfea.jpg\", \"size\": [149, 174]}'),('sorl-thumbnail||image||34c442b4c1ab681582f6035e326eba2c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/spec/person-example-2_1.png\", \"size\": [149, 174]}'),('sorl-thumbnail||image||dffd32b817ec4e9f90facb85c5025cb6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/dc/b4/dcb4a2c405e910f6e53966886bf9bcf1.png\", \"size\": [149, 174]}'),('sorl-thumbnail||thumbnails||34c442b4c1ab681582f6035e326eba2c','[\"dffd32b817ec4e9f90facb85c5025cb6\", \"129440d663bac05043535ae86309aca6\"]');
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-02 19:34:35
