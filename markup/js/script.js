var Spec = function(){
    $('.full-spec-link').on('click', function(e){
        e.preventDefault();

        var container = $(this).parents('.spec-item');
        var target = container.find('.spec-full');

        target.show();
    });

    $('.spec-full').on('click', '.modal-close', function(){
        var target = $(this).parents('.spec-full');

        target.hide();
    });
};

var Fancybox = function(){
    $('.fancybox').fancybox({
        padding: 0,
        tpl: {
            closeBtn: '<div class="modal-close"></div>'
        },
        helpers: {
            overlay: {
                opacity: 0.5,
                locked: false,
            }
        }
    });
};

$(function(){
    Fancybox();
    Spec();
});