# -*- coding: utf-8 -*-
import os
from fabric.api import *
from fabric.contrib import console
from fabric.colors import *
from fabric.contrib import django

#env.test_branch = 'test'
#env.dev_branch = 'dev'

env.hosts = ['root@62.76.184.19']
env.passwords = {'root@62.76.184.19':'6421S1309c'}
project_repository = 'https://grigl@bitbucket.org/grigl/asiaspa.git'
project_name = 'asiaspa'
mysql_password = 'jQ93Hb9'

def make_server():
    """
    Настройка сервера
    """
    local('apt-get update && apt-get upgrade')
    local('apt-get -y install mysql-server mysql-client python-software-properties python-dev python-pip python-setuptools python-mysqldb libxml2-dev sendmail nano wget libjpeg62 libjpeg62-dev zlib1g-dev libfreetype6 libfreetype6-dev')
    local('add-apt-repository ppa:cherokee-webserver && apt-get update && apt-get -y install cherokee libcherokee-mod-mysql libcherokee-mod-libssl libcherokee-mod-rrd libcherokee-mod-geoip')
    local('wget http://octoberweb.ru/static/fabric/Imaging-1.1.7.tar.gz && tar -xvfz Imaging-1.1.7.tar.gz && cd Imaging-1.1.7 && python install setup.py && cd .. &&& rm -R Imaging-1.1.7')
    local('pip install uwsgi==0.9.8.6 django==1.4.3 django-admin-tools sorl-thumbnail pytils django-mptt django-pagination django-simple-captcha pymorphy xhtml2pdf')
    local('wget http://octoberweb.ru/static/fabric/pymorphy.dict.ru.tar.gz && tar xvfz pymorphy.dict.ru.tar.gz && mkdir /usr/share/pymorphy && mv ru /usr/share/pymorphy')

def cmp(commit_text):
    local('git add .')
    local('git commit -m "%s"' % commit_text)
    local('git push origin master')

def cherokee_admin():
    run('cherokee-admin -u -b')

def deploy():
    with cd('/home/user/%s' % project_name):
        run('git fetch --all')
        run('git reset --hard origin/master')
        run("python manage.py migrate --ignore-ghost-migrations")
        run('touch reload.txt')

def first_deploy():
    with cd('/home/user/'):
        run('git clone %s' % project_repository)

    with cd('/home/user/%s' % project_name):
        # файлы проекта
        run('wget http://octoberweb.ru/static/fabric/django_wsgi.py')

        run('touch uwsgi.xml')
        run("echo '<uwsgi>' >> uwsgi.xml")
        run("echo '    <pythonpath>/home/user/%s</pythonpath>' >> uwsgi.xml" % project_name)
        run("echo '    <pythonpath>/home/user</pythonpath>' >> uwsgi.xml")
        run("echo '    <app mountpoint=\042/\042>' >> uwsgi.xml")
        run("echo '        <script>django_wsgi</script>' >> uwsgi.xml")
        run("echo '    </app>' >> uwsgi.xml")
        run("echo '    <touch-reload>/home/user/%s/reload.txt</touch-reload>' >> uwsgi.xml" % project_name)
        run("echo '</uwsgi>' >> uwsgi.xml")

        run('touch reload.txt')

        run('touch config/dbpass.py')
        dbpass = "DATABASE_PASSWORD = '%s'" % mysql_password
        run('echo "%s" >> config/dbpass.py' % dbpass)

        # бд
        run("mysql -u'root' -p'%s' -e 'create database %s character set utf8;'" % (mysql_password, project_name))
